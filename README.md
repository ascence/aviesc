# Project Aviesc (2015) #
- version 2.0

## MAIN REQUIREMENTS ##
1. python 3
2. django 1.8

## INSTALATION ##
1. python manage.py migrate
2a. python manage.py loaddata subscriptions.json
2b. python manage.py loaddata groups.json
2c. python manage.py loaddata engine_types.json
2d. python manage.py loaddata tax.json
3. go to /admin/sites/site/1/ and change data

## RUNNING ##

## TESTING ##

## DEVELOPMENT ##
1. update all pip packages `pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs pip install -U`

## DEVS ##
1. Michał Janowski (mjanowski@milosolutions.com) 
2. Jakub Wiśniowski (jwisniowski@milosolutions.com)