(function () {

  $("input[id^='signup_']").on('click', function (event) {
    event.preventDefault();
    $("#id_subscription").val($(this).data('sub-id'));
    $("#signup_form").submit();
  });

})(jQuery);