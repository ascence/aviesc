function formDisplay(freq, recurring_div) {
  if (freq == -1) { // no recurring
    recurring_div.hide();
  } else {
    recurring_div.show();
    if (freq == 3) { // rrule.DAILY
      $('#div_id_interval').show();
      $('#div_id_bymonthday').hide();
      $('#div_id_byweekday').hide();
      $('#div_id_every_n_month').hide();
      $('#id_interval').next('span').text('day(s)');
      $('#repeat_on').hide();
    } else if (freq == 2) { // rrule.WEEKLY
      $('#div_id_interval').show();
      $('#div_id_byweekday').show();
      $('#div_id_bymonthday').hide();
      $('#div_id_every_n_month').hide();
      $('#id_interval').next('span').text('week(s)');
      $('#repeat_on').show();
    } else if (freq == 1) { // rrule.MONTHLY
      $('#div_id_interval').hide();
      $('#div_id_byweekday').hide();
      $('#div_id_bymonthday').show();
      $('#div_id_every_n_month').show();
      $('#repeat_on').show();
    } else if (freq == 0) { // rrule.YEARLY
      $('#div_id_byweekday').hide();
      $('#div_id_bymonthday').hide();
      $('#div_id_every_n_month').hide();
      $('#id_interval').next('span').text('year(s)');
      $('#repeat_on').show();
    }
  }
}
$(document).ready(function () {
  if (isMaintenance) {
    $('fieldset:last').hide();
    return;
  }
  var recurring_div = $('#recurring');
  var freq_input = '#event-form input[name=freq]';
  var end_how_input = '#event-form input[name=end_how]';

  /* Show/Hide recurring div */
  formDisplay($(freq_input + ':checked').val(), recurring_div);

  if ($(end_how_input + ':checked').val() === 'times') {
    $('#div_id_count').show();
    $('#div_id_until').hide();
  } else {
    $('#div_id_count').hide();
    $('#div_id_until').show();
  }

  $(end_how_input).change(function () {
    if (this.value === 'times') {
      $('#div_id_count').show();
      $('#div_id_until').hide();
    } else {
      $('#div_id_count').hide();
      $('#div_id_until').show();
    }
  });

  $(freq_input).change(function () {
    formDisplay(this.value, recurring_div);
  });

  $('#id_maintenance').change(function () {
    if(this.value){
      formDisplay(-1, recurring_div);
      $(freq_input).val(['-1']);
    }
  });
});
/*
 $(document).ready(function () {
 var freq_input = '#event-form input[name=freq]';
 var end_how_input = '#event-form input[name=end_how]';
 var recurring_div = $('#recurring');
 var freq = $(freq_input + ':checked').val();

 if (freq === '') {
 recurring_div.hide();
 } else {
 recurring_div.show();
 hideAll();
 if (freq == '3') { // rrule.DAILY
 $('#daily_repeat_every').show();
 } else if (freq == '2') { // rrule.WEEKLY
 $('#weekly_repeat_every').show();
 $('#repeat_on').show();
 }
 }

 if ($(end_how_input + ':checked').val() === 'times') {
 $('#div_id_end_after').show();
 $('#div_id_end_until').hide();
 } else {
 $('#div_id_end_after').hide();
 $('#div_id_end_until').show();
 }

 $(freq_input).change(function () {
 hideAll();
 if (this.value === '') {
 recurring_div.hide();
 } else {
 recurring_div.show();
 console.log(this.value);
 if (this.value === '3') { // rrule.DAILY
 $('#daily_repeat_every').show();
 } else if (this.value === '2') { // rrule.WEEKLY
 $('#weekly_repeat_every').show();
 $('#repeat_on').show();
 }
 }
 });

 $(end_how_input).change(function () {
 if (this.value === 'times') {
 $('#div_id_end_after').show();
 $('#div_id_end_until').hide();
 } else {
 $('#div_id_end_after').hide();
 $('#div_id_end_until').show();
 }
 });
 });
 */
