$(function () {
  ($('select[name=pilot]').change(function () {
    $('input[name=pilot_name]').val($(this).find(":selected").text());
  }));

  ($('select[name=aircraft]').change(function () {
    var aircraft_pk = $(this).val();
    if (aircraft_pk) {
      $.ajax({
        url: "/invoice/api/aircraft/" + aircraft_pk + "/",
        method: "GET"
      }).done(function (data) {
        $('input[name=aircraft_mark]').val(data.aircraft_mark);
        $('input[name=aircraft_takeoff_cost]').val(data.takeoff_cost);
        $('input[name=aircraft_type]').val(data.aircraft_type_name);
      })
    }
  }));
})();
$(document).ready(function () {
  var itemsContainer = $('#items-form-container');
  var count = itemsContainer.children('div').length;
  $('#remove-last-item').click(function (ev) {
    count = itemsContainer.children('div').length;
    $('#id_invoice_extras-TOTAL_FORMS').attr('value', count - 1);
    $('div[id^="item-"]:last').remove();
  });
  $('#add-item').click(function (ev) {
    ev.preventDefault();
    count = itemsContainer.children('div').length;
    var tmplMarkup = $('#item-template').html();
    var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);
    itemsContainer.append(compiledTmpl);

    // update form count
    $('#id_invoice_extras-TOTAL_FORMS').attr('value', count + 1);

    // some animate to scroll to view our new form
    $('html, body').animate({
      scrollTop: $("#add-item-button").position().top - 200
    }, 800);
  });
});