function get_calendar_height() {
  return $(window).height() - 150;
}

function init_calendar(events_endpoint) {
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    editable: false,
    eventLimit: true,
    height: get_calendar_height(),
    events: function (start, end, timezone, callback) {
      $.ajax({
        url: events_endpoint,
        format: 'json',
        data: {
          start: start.format('YYYY-MM-DD'),
          end: end.format('YYYY-MM-DD')
        },
        success: function (events_data) {
          var events = [];
          events = $.merge(events, events_data);
          var recurr_event;
          for (var i = 0; i < events.length; i++) {
            events[i].className = events[i].is_maintenance == false ? ' ': 'event-maintenance ';
            events[i].className += events[i].confirmed ? 'confirmed': 'not-confirmed';
            for (var j = 0; j < events[i].dates.length; j++) {
              if (events[i].start != events[i].dates[j]) {
                recurr_event = jQuery.extend({}, events[i]);
                recurr_event.dates = [];
                recurr_event.start = (events[i].dates[j]);
                recurr_event.className = 'zxczxc';
                recurr_event.end = moment.utc(recurr_event.start)
                  .add(moment(events[i].end).diff(events[i].start, 'hours'), 'hours');
                events.push(recurr_event);
              }
            }
          }
          console.log(events);

          callback(events);
        }
      })
    }
  });
}