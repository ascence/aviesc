from django.contrib import admin

from .models import Aircraft, AircraftMaintenanceRule, LogEntry, AircraftMeterType, AircraftMeterTypeLog


class AircraftMaintenanceRuleInline(admin.TabularInline):
    model = AircraftMaintenanceRule
    extra = 1


class LogEntryInline(admin.TabularInline):
    model = LogEntry
    extra = 1


class AircraftMeterTypeInline(admin.TabularInline):
    model = AircraftMeterType
    extra = 1


class AircraftAdmin(admin.ModelAdmin):
    inlines = [
        AircraftMaintenanceRuleInline,
        LogEntryInline,
        AircraftMeterTypeInline,
    ]


admin.site.register(Aircraft, AircraftAdmin)
admin.site.register(AircraftMeterTypeLog)
