from django.db import models, DEFAULT_DB_ALIAS
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.validators import MinValueValidator
from django.conf import settings

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

from project_tango.core.models import BaseModel, CompanyAccount, User, Entity, AuditableModel
from project_tango.schedule.models import Calendar, AircraftEvent, EstimatedMeterValue, aircraft_event_post_save


def _date_now():
    return timezone.now()


class EngineType(Entity):
    pass


class Aircraft(AuditableModel):
    company_account = models.ForeignKey(CompanyAccount, related_name='aircrafts')
    # required
    registration_number = models.CharField(max_length=64, db_index=True)
    manufacturer = models.CharField(max_length=64)
    model = models.CharField(max_length=64)
    engine_type = models.ForeignKey(EngineType)
    # optional
    name = models.CharField(max_length=255, blank=True)
    hourly_rate = models.FloatField(null=True, blank=True, validators=[MinValueValidator(1.0)])
    maximum_passengers = models.PositiveIntegerField(null=True, blank=True)
    avg_speed = models.FloatField(null=True, blank=True, validators=[MinValueValidator(1.0)])
    # status
    is_active = models.BooleanField(default=True, editable=False)
    is_deleted = models.BooleanField(default=False, editable=False)

    def __str__(self):
        if self.name:
            return self.name
        return "{} {} ({})".format(self.manufacturer, self.model, self.registration_number, )

    def delete(self, using=DEFAULT_DB_ALIAS):
        """
        Set is_deleted flag. Don't delete any objects connected with it eg. logs
        :param using:
        """
        self.is_deleted = True
        return super(Aircraft, self).save()

    def delete_physical(self, using=DEFAULT_DB_ALIAS):
        """
        Pernamently delete Aircraft and all objects connected with it.
        :param using:
        """
        return super(Aircraft, self).delete(using)

    def check_maintenances(self):
        for maintenance in self.maintenances.all():
            if maintenance.auto_schedule is False:
                continue
            result = maintenance.predict_maintenance()
            if result:
                if hasattr(maintenance, 'aircraftevent'):
                    if not maintenance.aircraftevent.confirmed:
                        if maintenance.aircraftevent.start != result['start']:
                            maintenance.aircraftevent.start = result['start']
                            maintenance.aircraftevent.end = result['end']
                            maintenance.aircraftevent.save()
                    else:
                        # confirmed, finished maintenance, create new one
                        if maintenance.aircraftevent.start != result['start']:
                            post_save.disconnect(aircraft_event_post_save, sender=AircraftEvent)
                            ae_old = maintenance.aircraftevent
                            ae_old.maintenance = None
                            ae_old.save()
                            ae = AircraftEvent.objects.create(
                                calendar=self.calendar,
                                start=result['start'],
                                end=result['end'],
                                title=maintenance.description,
                                confirmed=False,
                                is_maintenance=True,
                            )
                            ae.maintenance = maintenance
                            ae.save()
                            post_save.connect(aircraft_event_post_save, sender=AircraftEvent)
                else:
                    AircraftEvent.objects.create(
                        calendar=self.calendar,
                        start=result['start'],
                        end=result['end'],
                        title=maintenance.description,
                        confirmed=False,
                        maintenance=maintenance,
                        is_maintenance=True,
                    )
            else:
                if hasattr(maintenance, 'aircraftevent'):
                    maintenance.aircraftevent.delete()


class AircraftMeterType(Entity):
    aircraft = models.ForeignKey(Aircraft, related_name='meter_types')
    allow_delete = models.BooleanField(default=True, editable=False)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class AircraftMaintenanceRule(AuditableModel):
    aircraft = models.ForeignKey(Aircraft, related_name='maintenances')
    description = models.TextField()
    meter_type = models.ForeignKey(AircraftMeterType, null=True, blank=True)
    meter_increment = models.PositiveIntegerField(null=True, blank=True)
    calendar_increment = models.PositiveIntegerField(null=True, blank=True)
    trigger_threshold = models.PositiveIntegerField(default=10)
    auto_schedule = models.BooleanField(default=False, verbose_name=_("Auto-Schedule"))
    downtime_duration = models.IntegerField(default=1, verbose_name=_("Downtime (hrs)"))
    raise_alert = models.BooleanField(default=True, verbose_name=_("Alert"))

    def __str__(self):
        return "{} for {}".format(self.description, self.aircraft)

    def last_maintenance(self):
        return self.logentry_set.order_by('-entry_time').first()

    def log_entries_after_last_maintenance(self):
        last = self.last_maintenance()
        log_entries = LogEntry.objects.filter(aircraft=self.aircraft)
        if last:
            return log_entries.filter(entry_time__gt=last.entry_time)

        return log_entries

    def predict_maintenance(self):
        """
        Calculate next maintenance date suit to free slot and profile maintenance settings
        :return {'start': datetime, 'end': datetime} or None:
        """
        company_account = self.aircraft.company_account
        if hasattr(self, 'aircraftevent'):
            maintenance_event = self.aircraftevent
        else:
            maintenance_event = None
        if self.calendar_increment:
            last_maintenance = self.last_maintenance()
            if last_maintenance:
                next_start = last_maintenance.entry_time + timedelta(days=self.calendar_increment)
            else:
                next_start = timezone.now().replace(
                    hour=company_account.maint_work_start_hr.hour,
                    minute=company_account.maint_work_start_hr.minute,
                    second=0
                )
            next_end = next_start + timedelta(hours=self.downtime_duration)
            result = self.aircraft.calendar.check_or_find_slot(
                start=next_start, end=next_end, maintenance_event=maintenance_event)
            return result

        elif self.meter_increment:
            # calculate the total meter value between the last maintenance date (lmd) and now
            result = self.trigger()
            trigger = result['trigger']
            amount = result['amount']
            if trigger:
                next_start = timezone.now().replace(
                    hour=company_account.maint_work_start_hr.hour,
                    minute=company_account.maint_work_start_hr.minute,
                    second=0
                )
                next_end = next_start + timedelta(hours=self.downtime_duration)
                result = self.aircraft.calendar.check_or_find_slot(
                    start=next_start, end=next_end, maintenance_event=maintenance_event)
                return result
            else:
                # check future events
                try:
                    last_log_entry_date = self.aircraft.aircraft_log.filter(
                        maintenance_rule__isnull=True).order_by('-entry_time').first().entry_time
                    future_events = self.aircraft.calendar.events.filter(
                        maintenance__isnull=True, start__gt=last_log_entry_date)
                except AttributeError:
                    future_events = self.aircraft.calendar.events.filter(is_maintenance=False)
                for event in future_events:
                    try:
                        amount += event.custom_parameters.get(meter_type=self.meter_type).value
                    except EstimatedMeterValue.DoesNotExist:
                        amount += 0
                    if amount >= self.meter_increment - (self.trigger_threshold / 100 * self.meter_increment):
                        next_start = event.end
                        next_end = next_start + timedelta(hours=self.downtime_duration)
                        result = self.aircraft.calendar.check_or_find_slot(
                            start=next_start, end=next_end, maintenance_event=maintenance_event)
                        return result

        else:
            pass

    def trigger(self):
        """
        Check if maintenance should trigger maintenance requirement responses
        :return True if yes, False otherwise:
        """
        if self.meter_increment:
            amount_from_logs = 0
            for log in self.log_entries_after_last_maintenance():
                try:
                    amount_from_logs += log.custom_parameters.filter(
                        meter_type=self.meter_type).aggregate(models.Sum('value'))['value__sum']
                except TypeError:
                    pass
            return {
                'trigger': amount_from_logs >= self.meter_increment - (
                    self.trigger_threshold / 100 * self.meter_increment),
                'amount': amount_from_logs,
            }
        elif self.calendar_increment:
            last_maintenance = self.last_maintenance()
            if last_maintenance:
                diff = (timezone.now() - last_maintenance.entry_time).days
                return {
                    'trigger': diff >= self.calendar_increment - (
                        self.trigger_threshold / 100 * self.calendar_increment),
                    'amount': diff
                }
            else:
                return {'trigger': True, 'amount': 0}
        else:
            return {'trigger': False, 'amount': 0}


class LogEntry(AuditableModel):
    # Store either as an aircraft log entry, pilot log entry or both
    aircraft = models.ForeignKey(Aircraft, related_name='aircraft_log')
    entry_time = models.DateTimeField(db_index=True)
    # If this is a flight entry
    event = models.ForeignKey(AircraftEvent, blank=True, null=True, related_name='event_logs',
                              help_text=_("Choose if it's flight log"))
    pilot = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name="pilot_aircraft_logs")
    # If this is a maintenance entry
    maintenance_rule = models.ForeignKey(AircraftMaintenanceRule, blank=True, null=True,
                                         help_text=_("Choose if it's maintenance log"))

    # Any sundry comments or links to sqawks raised
    comments = models.TextField()

    def __str__(self):
        return "{} for {} on {}".format(self.comments, self.aircraft, self.entry_time)

    class Meta:
        ordering = ['entry_time', ]


class AircraftMeterTypeLog(models.Model):
    log_entry = models.ForeignKey(LogEntry, related_name='custom_parameters')
    meter_type = models.ForeignKey(AircraftMeterType)
    value = models.FloatField()

    class Meta:
        ordering = ('meter_type__name',)


# Signals ##############################################################################################################


@receiver(post_save, sender=Aircraft)
def aircraft_post_save(sender, instance, **kwargs):
    if kwargs['created']:
        # Create aircraft calendar for it
        Calendar.objects.create(aircraft=instance, name=instance)
        # Every aircraft has `Flying hours` and `engine starts` meter type by default
        AircraftMeterType.objects.create(aircraft=instance, name='Flying hour(s)', allow_delete=False)
        AircraftMeterType.objects.create(aircraft=instance, name='Engine Starts', allow_delete=False)


@receiver(post_save, sender=AircraftMaintenanceRule)
def aircraft_maintenance_rule_post_save(sender, instance, **kwargs):
    company = instance.aircraft.company_account
    calendar = instance.aircraft.calendar
    if kwargs['created']:
        if instance.auto_schedule is True:
            # create event for maintenance in date
            # start date: maintenance date create
            # start hour: first hour set in profile settings
            # if there is no slot check previous day
            result = instance.predict_maintenance()
            if result:
                AircraftEvent.objects.create(
                    calendar=calendar,
                    start=result['start'],
                    end=result['end'],
                    title=instance.description,
                    confirmed=False,
                    maintenance=instance,
                    is_maintenance=True,
                )
    else:
        pass


@receiver(post_save, sender=LogEntry)
def aircraft_log_post_save(sender, instance, **kwargs):
    if kwargs['created']:
        # confirm maintenance if log entry is created for it
        if instance.maintenance_rule is not None:  # created log for maintenance
            if hasattr(instance.maintenance_rule, 'aircraftevent'):
                event_maintenance = instance.maintenance_rule.aircraftevent
                if event_maintenance:
                    event_maintenance.confirmed = True
                    event_maintenance.save()

    # check if system can aircraft maintenance forecast
    instance.aircraft.check_maintenances()
