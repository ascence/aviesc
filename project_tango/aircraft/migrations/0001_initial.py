# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Aircraft',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_ip', models.CharField(editable=False, max_length=45)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_ip', models.CharField(editable=False, max_length=45)),
                ('registration_number', models.CharField(max_length=64, db_index=True)),
                ('manufacturer', models.CharField(max_length=64)),
                ('model', models.CharField(max_length=64)),
                ('name', models.CharField(max_length=255, blank=True)),
                ('hourly_rate', models.FloatField(null=True, validators=[django.core.validators.MinValueValidator(1.0)], blank=True)),
                ('maximum_passengers', models.PositiveIntegerField(null=True, blank=True)),
                ('avg_speed', models.FloatField(null=True, validators=[django.core.validators.MinValueValidator(1.0)], blank=True)),
                ('is_active', models.BooleanField(editable=False, default=True)),
                ('is_deleted', models.BooleanField(editable=False, default=False)),
                ('company_account', models.ForeignKey(related_name='aircrafts', to='core.CompanyAccount')),
                ('created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, editable=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AircraftMaintenanceRule',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_ip', models.CharField(editable=False, max_length=45)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_ip', models.CharField(editable=False, max_length=45)),
                ('description', models.TextField()),
                ('meter_increment', models.PositiveIntegerField(null=True, blank=True)),
                ('calendar_increment', models.PositiveIntegerField(null=True, blank=True)),
                ('trigger_threshold', models.PositiveIntegerField(default=10)),
                ('auto_schedule', models.BooleanField(default=False, verbose_name='Auto-Schedule')),
                ('downtime_duration', models.IntegerField(default=1, verbose_name='Downtime (hrs)')),
                ('raise_alert', models.BooleanField(default=True, verbose_name='Alert')),
                ('aircraft', models.ForeignKey(related_name='maintenances', to='aircraft.Aircraft')),
                ('created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, editable=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AircraftMeterType',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('aircraft', models.ForeignKey(related_name='meter_types', to='aircraft.Aircraft')),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='AircraftMeterTypeLog',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('value', models.FloatField()),
            ],
            options={
                'ordering': ('meter_type__name',),
            },
        ),
        migrations.CreateModel(
            name='EngineType',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='LogEntry',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_ip', models.CharField(editable=False, max_length=45)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_ip', models.CharField(editable=False, max_length=45)),
                ('entry_time', models.DateTimeField(db_index=True)),
                ('engine_starts', models.PositiveIntegerField(default=0)),
                ('comments', models.TextField()),
                ('aircraft', models.ForeignKey(related_name='aircraft_log', to='aircraft.Aircraft')),
                ('created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, editable=False)),
                ('maintenance_rule', models.ForeignKey(to='aircraft.AircraftMaintenanceRule', blank=True, null=True)),
                ('modified_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, editable=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='aircraftmetertypelog',
            name='log_entry',
            field=models.ForeignKey(related_name='custom_parameters', to='aircraft.LogEntry'),
        ),
        migrations.AddField(
            model_name='aircraftmetertypelog',
            name='meter_type',
            field=models.ForeignKey(to='aircraft.AircraftMeterType'),
        ),
        migrations.AddField(
            model_name='aircraftmaintenancerule',
            name='meter_type',
            field=models.ForeignKey(to='aircraft.AircraftMeterType', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='aircraftmaintenancerule',
            name='modified_by',
            field=models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, editable=False),
        ),
        migrations.AddField(
            model_name='aircraft',
            name='engine_type',
            field=models.ForeignKey(to='aircraft.EngineType'),
        ),
        migrations.AddField(
            model_name='aircraft',
            name='modified_by',
            field=models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, editable=False),
        ),
    ]
