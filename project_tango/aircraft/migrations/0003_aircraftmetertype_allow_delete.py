# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aircraft', '0002_remove_logentry_engine_starts'),
    ]

    operations = [
        migrations.AddField(
            model_name='aircraftmetertype',
            name='allow_delete',
            field=models.BooleanField(default=True),
        ),
    ]
