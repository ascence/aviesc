# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0003_auto_20150829_1554'),
        ('aircraft', '0004_auto_20150828_1642'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='logentry',
            options={'ordering': ['entry_time']},
        ),
        migrations.AddField(
            model_name='logentry',
            name='event',
            field=models.ForeignKey(to='schedule.AircraftEvent', blank=True, related_name='event_logs', help_text="Choose if it's flight log", null=True),
        ),
        migrations.AlterField(
            model_name='logentry',
            name='maintenance_rule',
            field=models.ForeignKey(to='aircraft.AircraftMaintenanceRule', blank=True, help_text="Choose if it's maintenance log", null=True),
        ),
    ]
