# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('aircraft', '0003_aircraftmetertype_allow_delete'),
    ]

    operations = [
        migrations.AddField(
            model_name='logentry',
            name='pilot',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True, null=True, related_name='pilot_aircraft_logs'),
        ),
        migrations.AlterField(
            model_name='aircraftmetertype',
            name='allow_delete',
            field=models.BooleanField(editable=False, default=True),
        ),
    ]
