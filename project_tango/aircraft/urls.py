from django.conf.urls import include, url

from .views import AircraftList, AircraftCreate, AircraftDelete, AircraftUpdate, AircraftMaintenanceRuleCreate, \
    AircraftMaintenanceRuleUpdate, AircraftMaintenanceRuleDelete, AircraftLogEntryCreate, AircraftLogEntryDelete, \
    AircraftLogEntryUpdate, AircraftMeterTypeCreate, AircraftMeterTypeUpdate, AircraftMeterTypeDelete

urlpatterns = [
    url(r'^$', AircraftList.as_view(), name="aircraft-list"),
    url(r'^create/$', AircraftCreate.as_view(), name="aircraft-create"),
    url(r'^(?P<pk>[\w]+)/update/$', AircraftUpdate.as_view(), name="aircraft-update"),
    url(r'^(?P<pk>[\w]+)/delete/$', AircraftDelete.as_view(), name="aircraft-delete"),
    url(r'^(?P<aircraft_pk>[\w]+)/maintenance/create/$', AircraftMaintenanceRuleCreate.as_view(),
        name="aircraft-maintenance-create"),
    url(r'^(?P<aircraft_pk>[\w]+)/maintenance/(?P<pk>[\w]+)/update/$', AircraftMaintenanceRuleUpdate.as_view(),
        name="aircraft-maintenance-update"),
    url(r'^(?P<aircraft_pk>[\w]+)/maintenance/(?P<pk>[\w]+)/delete/$', AircraftMaintenanceRuleDelete.as_view(),
        name="aircraft-maintenance-delete"),
    url(r'^(?P<aircraft_pk>[\w]+)/log/create/$', AircraftLogEntryCreate.as_view(),
        name="aircraft-log-entry-create"),
    url(r'^(?P<aircraft_pk>[\w]+)/log/(?P<pk>[\w]+)/update/$', AircraftLogEntryUpdate.as_view(),
        name="aircraft-log-entry-update"),
    url(r'^(?P<aircraft_pk>[\w]+)/log/(?P<pk>[\w]+)/delete/$', AircraftLogEntryDelete.as_view(),
        name="aircraft-log-entry-delete"),
    url(r'^(?P<aircraft_pk>[\w]+)/meter_type/create/$', AircraftMeterTypeCreate.as_view(),
        name="aircraft-meter-type-create"),
    url(r'^(?P<aircraft_pk>[\w]+)/meter_type/(?P<pk>[\w]+)/update/$', AircraftMeterTypeUpdate.as_view(),
        name="aircraft-meter-type-update"),
    url(r'^(?P<aircraft_pk>[\w]+)/meter_type/(?P<pk>[\w]+)/delete/$', AircraftMeterTypeDelete.as_view(),
        name="aircraft-meter-type-delete"),
]
