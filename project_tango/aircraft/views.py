from django.core.urlresolvers import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import permission_required
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.text import slugify

from django.utils import timezone
from datetime import datetime

from .models import Aircraft, AircraftMaintenanceRule, LogEntry, AircraftMeterType, AircraftMeterTypeLog
from .forms import AircraftCreateForm, AircraftMaintenanceRuleForm, AircraftLogEntryForm, AircraftMeterTypeForm
from project_tango.core.permissions import UserPermissions
from project_tango.schedule.models import Calendar, AircraftEvent


class AircraftList(ListView):
    model = Aircraft
    template_name = 'dashboard/aircraft/aircraft_list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AircraftList, self).dispatch(request)

    def get_queryset(self):
        return Aircraft.objects.filter(is_deleted=False, company_account=self.request.user.company_account)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subscription'] = self.request.user.company_account.subscription
        context['amount_of_aircraft'] = self.get_queryset().count()
        maintenances = AircraftMaintenanceRule.objects.filter(aircraft__in=self.get_queryset())
        context['close_maintenances'] = list()
        for maintenance in maintenances:
            if maintenance.trigger()['trigger']:
                context['close_maintenances'].append(maintenance)
        return context


class AircraftCreate(CreateView):
    model = Aircraft
    template_name = 'dashboard/aircraft/aircraft_create.html'
    form_class = AircraftCreateForm
    success_url = ''
    success_message = _("Aircraft was created successfully")

    @method_decorator(login_required)
    @method_decorator(permission_required('aircraft.add_aircraft'))
    def dispatch(self, request, *args, **kwargs):
        if not UserPermissions.has_subscription_aircraft_limit(request.user):
            messages.error(self.request, _("Sorry, you've reached your limit! Extend your subscription."))
            return HttpResponseRedirect(self.success_url)
        return super(AircraftCreate, self).dispatch(request)

    def get_form_kwargs(self):
        kwargs = super(AircraftCreate, self).get_form_kwargs()
        kwargs.update({'initial': {'company_account': self.request.user.company_account.pk}})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('aircraft-update', args=[self.object.pk])

    def form_valid(self, form):
        self.object = form.save()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())


class AircraftDelete(DeleteView):
    model = Aircraft
    success_url = reverse_lazy('aircraft-list')
    success_message = _("Aircraft was deleted successfully")

    @method_decorator(login_required)
    @method_decorator(permission_required('aircraft.delete_aircraft'))
    def dispatch(self, request, *args, **kwargs):
        return super(AircraftDelete, self).dispatch(request)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.success_url)


class AircraftUpdate(UpdateView):
    model = Aircraft
    template_name = 'dashboard/aircraft/aircraft_update.html'
    form_class = AircraftCreateForm
    success_url = reverse_lazy('aircraft-list')
    success_message = "Aircraft was updated successfully"

    @method_decorator(login_required)
    @method_decorator(permission_required('aircraft.change_aircraft'))
    def dispatch(self, request, *args, **kwargs):
        try:
            aircraft = Aircraft.objects.get(id=kwargs['pk'], company_account=request.user.company_account)
        except Aircraft.DoesNotExist:
            messages.error(self.request, _("You haven't got permissions to edit this aircraft."))
            return HttpResponseRedirect(self.success_url)
        self.object = aircraft
        return super(AircraftUpdate, self).dispatch(request)

    def get_form_kwargs(self):
        kwargs = super(AircraftUpdate, self).get_form_kwargs()
        kwargs.update({'initial': {'company_account': self.request.user.company_account.pk}})
        return kwargs

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        return super(AircraftUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(AircraftUpdate, self).get_context_data(**kwargs)
        context['maintenances'] = AircraftMaintenanceRule.objects.filter(aircraft=self.object)
        context['logs'] = self.object.aircraft_log.all().order_by('-entry_time')
        context['meter_types'] = self.object.meter_types.all()
        return context


class AircraftMaintenanceRuleCreate(CreateView):
    model = AircraftMaintenanceRule
    template_name = 'dashboard/aircraft/aircraft_maintenance_rule.html'
    form_class = AircraftMaintenanceRuleForm
    success_url = ''
    success_message = _("Aircraft Maintenance was created successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not Aircraft.objects.filter(id=kwargs['aircraft_pk'], company_account=request.user.company_account).exists():
            messages.error(self.request, _("You haven't got permissions to edit this aircraft maintenance."))
            return HttpResponseRedirect(self.success_url)
        return super(AircraftMaintenanceRuleCreate, self).dispatch(request)

    def get_form_kwargs(self):
        aircraft_pk = self.kwargs['aircraft_pk']
        kwargs = super(AircraftMaintenanceRuleCreate, self).get_form_kwargs()
        kwargs.update({'initial': {'aircraft': aircraft_pk}})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AircraftMaintenanceRuleCreate, self).get_context_data(**kwargs)
        context['aircraft_pk'] = self.kwargs['aircraft_pk']
        return context

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        return super(AircraftMaintenanceRuleCreate, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('aircraft-update', args=[self.object.aircraft.pk])


class AircraftMaintenanceRuleUpdate(UpdateView):
    model = AircraftMaintenanceRule
    template_name = 'dashboard/aircraft/aircraft_maintenance_rule.html'
    form_class = AircraftMaintenanceRuleForm
    success_url = ''
    success_message = _("Aircraft Maintenance was updated successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not Aircraft.objects.filter(id=kwargs['aircraft_pk'], company_account=request.user.company_account).exists():
            messages.error(self.request, _("You haven't got permissions to edit this aircraft maintenance."))
            return HttpResponseRedirect(self.success_url)
        return super(AircraftMaintenanceRuleUpdate, self).dispatch(request)

    def get_context_data(self, **kwargs):
        context = super(AircraftMaintenanceRuleUpdate, self).get_context_data(**kwargs)
        context['aircraft_pk'] = self.kwargs['aircraft_pk']
        return context

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        return super(AircraftMaintenanceRuleUpdate, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('aircraft-update', args=[self.object.aircraft.pk])


class AircraftMaintenanceRuleDelete(DeleteView):
    model = AircraftMaintenanceRule
    success_url = ''
    success_message = _("Aircraft Maintenance was deleted successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AircraftMaintenanceRuleDelete, self).dispatch(request)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('aircraft-update', args=[self.object.aircraft.pk])


class AircraftLogEntryCreate(CreateView):
    model = LogEntry
    template_name = 'dashboard/aircraft/aircraft_log_entry.html'
    form_class = AircraftLogEntryForm
    success_url = ''
    success_message = _("Log entry was created successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not Aircraft.objects.filter(id=kwargs['aircraft_pk'], company_account=request.user.company_account).exists():
            messages.error(self.request, _("You haven't got permissions to edit this aircraft maintenance."))
            return HttpResponseRedirect(self.success_url)
        return super(AircraftLogEntryCreate, self).dispatch(request)

    def get_form_kwargs(self):
        aircraft_pk = self.kwargs['aircraft_pk']
        kwargs = super(AircraftLogEntryCreate, self).get_form_kwargs()
        kwargs.update({'initial': {'aircraft': aircraft_pk}})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AircraftLogEntryCreate, self).get_context_data(**kwargs)
        context['aircraft_pk'] = self.kwargs['aircraft_pk']
        context['aircraft'] = get_object_or_404(Aircraft, pk=self.kwargs['aircraft_pk'])
        context['form'].fields['pilot'].queryset = self.request.user.company_account.company_pilots()
        context['form'].fields['maintenance_rule'].queryset = context['aircraft'].maintenances.all()
        context['custom_parameters'] = context['aircraft'].meter_types.all()
        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        if form.is_valid():
            for (k, val) in request.POST.items():
                if k.startswith('param') and not val:
                    messages.error(self.request, _('Please, fill all parameters'))
                    return self.render_to_response(self.get_context_data(form=form))
            self.object = form.save()
        else:
            return self.render_to_response(self.get_context_data(form=form))

        for (k, val) in request.POST.items():
            if k.startswith('param'):
                if val:
                    param_id = k.split('_')[1]
                    try:
                        param = AircraftMeterType.objects.get(id=param_id)
                        AircraftMeterTypeLog.objects.create(log_entry=self.object, meter_type=param, value=val)
                    except (AttributeError, ValueError, AircraftMeterType.DoesNotExist):
                        pass
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('aircraft-update', args=[self.object.aircraft.pk])


class AircraftLogEntryUpdate(UpdateView):
    model = LogEntry
    template_name = 'dashboard/aircraft/aircraft_log_entry_update.html'
    form_class = AircraftLogEntryForm
    success_url = ''
    success_message = _("Aircraft Log Entry was updated successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not Aircraft.objects.filter(id=kwargs['aircraft_pk'], company_account=request.user.company_account).exists():
            messages.error(self.request, _("You haven't got permissions to edit this aircraft maintenance."))
            return HttpResponseRedirect(self.success_url)
        return super(AircraftLogEntryUpdate, self).dispatch(request)

    def get_context_data(self, **kwargs):
        context = super(AircraftLogEntryUpdate, self).get_context_data(**kwargs)
        context['aircraft_pk'] = self.kwargs['aircraft_pk']
        context['aircraft'] = get_object_or_404(Aircraft, pk=self.kwargs['aircraft_pk'])
        context['form'].fields['pilot'].queryset = self.request.user.company_account.company_pilots()
        context['form'].fields['maintenance_rule'].queryset = context['aircraft'].maintenances.all()
        return context

    def get_success_url(self):
        return reverse_lazy('aircraft-update', args=[self.object.aircraft.pk])

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            # validate
            for (k, val) in request.POST.items():
                if k.startswith('param') and not val:
                    messages.error(self.request, _('Please, fill all parameters'))
                    return self.render_to_response(self.get_context_data(form=form))
            # save parameters
            for (k, val) in request.POST.items():
                if k.startswith('param'):
                    if val:
                        param_id = k.split('_')[1]
                        try:
                            param = AircraftMeterTypeLog.objects.get(id=param_id)
                            param.value = val
                            param.save()
                        except (AttributeError, ValueError, AircraftMeterTypeLog.DoesNotExist):
                            pass
            self.object = form.save()
        else:
            return self.render_to_response(self.get_context_data(form=form))

        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())


class AircraftLogEntryDelete(DeleteView):
    model = LogEntry
    success_url = ''
    success_message = _("Aircraft Log entry was deleted successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AircraftLogEntryDelete, self).dispatch(request)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('aircraft-update', args=[self.object.aircraft.pk])


class AircraftMeterTypeCreate(CreateView):
    model = LogEntry
    template_name = 'dashboard/aircraft/aircraft_meter_type.html'
    form_class = AircraftMeterTypeForm
    success_url = ''
    success_message = _("Log entry was created successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        aircraft = get_object_or_404(Aircraft, pk=kwargs['aircraft_pk'])

        if aircraft.meter_types.count() >= 5:
            messages.error(self.request, _("One aircraft can have only 5 meter types assigned."))
            return HttpResponseRedirect(reverse_lazy('aircraft-update', args=[aircraft.pk]))
        return super(AircraftMeterTypeCreate, self).dispatch(request)

    def get_form_kwargs(self):
        aircraft_pk = self.kwargs['aircraft_pk']
        kwargs = super(AircraftMeterTypeCreate, self).get_form_kwargs()
        kwargs.update({'initial': {'aircraft': aircraft_pk}})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AircraftMeterTypeCreate, self).get_context_data(**kwargs)
        context['aircraft_pk'] = self.kwargs['aircraft_pk']
        context['aircraft'] = get_object_or_404(Aircraft, pk=self.kwargs['aircraft_pk'])
        return context

    def get_success_url(self):
        return reverse_lazy('aircraft-update', args=[self.object.aircraft.pk])


class AircraftMeterTypeUpdate(UpdateView):
    model = AircraftMeterType
    template_name = 'dashboard/aircraft/aircraft_meter_type.html'
    form_class = AircraftMeterTypeForm
    success_url = ''
    success_message = _("Aircraft Meter Type was updated successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AircraftMeterTypeUpdate, self).dispatch(request)

    def get_context_data(self, **kwargs):
        context = super(AircraftMeterTypeUpdate, self).get_context_data(**kwargs)
        context['aircraft_pk'] = self.kwargs['aircraft_pk']
        context['aircraft'] = get_object_or_404(Aircraft, pk=self.kwargs['aircraft_pk'])
        return context

    def get_success_url(self):
        return reverse_lazy('aircraft-update', args=[self.object.aircraft.pk])


class AircraftMeterTypeDelete(DeleteView):
    model = AircraftMeterType
    success_url = ''
    success_message = _("Aircraft Meter type was deleted successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AircraftMeterTypeDelete, self).dispatch(request)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('aircraft-update', args=[self.object.aircraft.pk])
