from django.test import TestCase
from django.utils import timezone

from datetime import datetime, timedelta


from .models import Aircraft, EngineType, AircraftMaintenanceRule, LogEntry, AircraftMeterType, AircraftMeterTypeLog
from project_tango.core.models import CompanyAccount, User


class TestMaintenance(TestCase):
    def setUp(self):
        self.engine1 = EngineType.objects.create(name='engine 1')
        self.company_account = CompanyAccount.objects.create(
            company_name='company_name',
        )
        self.user = User.objects.create(
            email='email@example.com',
            first_name='first', last_name='last',
            company_account=self.company_account
        )
        self.aircraft1 = Aircraft.objects.create(
            company_account=self.company_account,
            registration_number=123,
            manufacturer='manufacturer',
            model='model',
            engine_type=self.engine1,
            name='Aircraft 1',
            created_by=self.user,
            modified_by=self.user,
        )

    def test_calendar_trigger(self):
        calendar_maintenance1 = AircraftMaintenanceRule.objects.create(
            aircraft=self.aircraft1,
            description='Routine Control',
            calendar_increment=10,
            trigger_threshold=20,
            auto_schedule=True,
            raise_alert=True,
            downtime_duration=2,
            created_by=self.user,
            modified_by=self.user,
        )
        # after create maintenance with calendar increment event for maintenace is created
        self.assertTrue(hasattr(calendar_maintenance1, 'aircraftevent'))

        # no previous maintenance in log -> True
        self.assertTrue(calendar_maintenance1.trigger())

        # last maintenance was 6days ago
        le = LogEntry.objects.create(
            aircraft=self.aircraft1,
            entry_time=timezone.now() - timedelta(days=6),
            maintenance_rule=calendar_maintenance1,
            comments='Maintenance complete'
        )
        # False because it should trigger 2 days before (20% of 10)
        self.assertFalse(calendar_maintenance1.trigger())

        le.entry_time = timezone.now() - timedelta(days=8)
        le.save()
        # True because last maintenance was 8 days ago (2day to next, 20% of 10)
        self.assertTrue(calendar_maintenance1.trigger())

        LogEntry.objects.create(
            aircraft=self.aircraft1,
            entry_time=timezone.now(),
            maintenance_rule=calendar_maintenance1,
            comments='Maintenance complete'
        )
        # False because last maintenance just created
        self.assertFalse(calendar_maintenance1.trigger())

    def test_meter_trigger(self):
        engine_starts_type = AircraftMeterType.objects.get(aircraft=self.aircraft1, name='Engine Starts')

        meter_maintenance1 = AircraftMaintenanceRule.objects.create(
            aircraft=self.aircraft1,
            description='Change Oil every 10 engine starts',
            meter_type=engine_starts_type,
            meter_increment=10,
            trigger_threshold=20,
            auto_schedule=True,
            raise_alert=True,
            downtime_duration=2,
            created_by=self.user,
            modified_by=self.user,
        )
        # after create maintenance with meter increment and no logs event for maintenace is not created
        self.assertFalse(hasattr(meter_maintenance1, 'aircraftevent'))

        # no logs -> False
        self.assertFalse(meter_maintenance1.trigger())

        le1 = LogEntry.objects.create(
            aircraft=self.aircraft1,
            entry_time=timezone.now(),
            comments='Flight Event'
        )
        AircraftMeterTypeLog.objects.create(log_entry=le1, meter_type=engine_starts_type, value=4)

        # 4 engine starts. Require 8 to trigger -> False
        self.assertFalse(meter_maintenance1.trigger())

        AircraftMeterTypeLog.objects.create(log_entry=le1, meter_type=engine_starts_type, value=4)

        # 8 engine starts. Require 8 to trigger -> True
        self.assertTrue(meter_maintenance1.trigger())

        LogEntry.objects.create(
            aircraft=self.aircraft1,
            entry_time=timezone.now(),
            comments='Flight Event',
            maintenance_rule=meter_maintenance1,
        )
        # maintenance completed. 0 engine starts. Require 8 to trigger -> False
        self.assertFalse(meter_maintenance1.trigger())
