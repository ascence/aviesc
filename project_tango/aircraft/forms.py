from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.core.exceptions import ValidationError

from datetimewidget.widgets import DateTimeWidget

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, HTML, Fieldset
from crispy_forms.bootstrap import Div, FormActions, InlineCheckboxes, AppendedText, PrependedAppendedText

from project_tango.aircraft.models import Aircraft, AircraftMaintenanceRule, LogEntry, AircraftMeterType
from project_tango.core.models import CompanyAccount


class AircraftCreateForm(forms.ModelForm):
    company_account = forms.ModelChoiceField(widget=forms.HiddenInput, queryset=CompanyAccount.objects.all())

    class Meta:
        model = Aircraft
        exclude = ('is_deleted',)

    def __init__(self, *args, **kwargs):
        super(AircraftCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('company_account'),
            Div(
                Div(Field('registration_number'), css_class='col-md-4'),
                css_class='row'
            ),
            Div(
                Div(Field('manufacturer'), css_class='col-md-4'),
                Div(Field('model'), css_class='col-md-4'),
                Div(Field('engine_type'), css_class='col-md-4'),
                css_class="row"
            ),
            Fieldset(
                "Optional",
                Div(
                    Div(Field('name'), css_class='col-md-4'),
                    css_class='row'
                ),
                Div(
                    Div(Field('hourly_rate'), css_class='col-md-4'),
                    Div(Field('avg_speed'), css_class='col-md-4'),
                    Div(Field('maximum_passengers'), css_class='col-md-4'),
                    css_class='row'
                )
            )
        )


class AircraftMaintenanceRuleForm(forms.ModelForm):
    aircraft = forms.ModelChoiceField(widget=forms.HiddenInput, queryset=Aircraft.objects.all())

    class Meta:
        model = AircraftMaintenanceRule
        fields = ('aircraft', 'description', 'meter_type', 'meter_increment', 'calendar_increment',
                  'auto_schedule', 'downtime_duration', 'raise_alert', 'trigger_threshold')
        widgets = {
            'description': forms.Textarea(attrs={'rows': 2}),
        }

    def __init__(self, *args, **kwargs):
        super(AircraftMaintenanceRuleForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('aircraft'),
            Div(
                Div(Field('description'), css_class="col-md-12"),
                css_class="row"
            ),
            Fieldset(
                'Meter increment',
                Div(
                    Div(Field('meter_type'), css_class="col-md-4"),
                    Div(Field('meter_increment'), css_class="col-md-4"),

                    css_class="row"
                ),
            ),
            Fieldset(
                'Calendar increment',
                Div(
                    Div(PrependedAppendedText('calendar_increment', _('every'), _('days')), css_class="col-md-4"),

                    css_class="row"
                ),
            ),
            Fieldset(
                'Other',
                Div(
                    Div(Field('downtime_duration'), css_class="col-md-4"),
                    Div(AppendedText('trigger_threshold', '%'), css_class="col-md-4"),
                    css_class="row"
                ),
                Div(
                    Div(Field('raise_alert'), css_class="col-md-4"),
                    Div(Field('auto_schedule'), css_class="col-md-4"),

                    css_class="row"
                )
            )
        )

    def clean(self):
        cleaned_data = super(AircraftMaintenanceRuleForm, self).clean()
        if cleaned_data['meter_increment'] and cleaned_data['calendar_increment']:
            raise ValidationError(_('You have to selected `meter increment` or `calendar increment`'))
        if cleaned_data['meter_increment'] and not cleaned_data['meter_type']:
            raise ValidationError({'meter_type': _('Give `meter type` if you selected `meter increment`.')})
        if cleaned_data['meter_type'] and not cleaned_data['meter_increment']:
            raise ValidationError({'meter_increment': _('Give `meter increment` if you selected `meter type`.')})


class AircraftLogEntryForm(forms.ModelForm):
    aircraft = forms.ModelChoiceField(widget=forms.HiddenInput, queryset=Aircraft.objects.all())

    class Meta:
        model = LogEntry
        fields = ('aircraft', 'entry_time', 'pilot', 'event', 'maintenance_rule', 'comments',)
        widgets = {
            'comments': forms.Textarea(attrs={'rows': 2}),
            'entry_time': DateTimeWidget(bootstrap_version=3, options={
                'minuteStep': 15,
                'format': 'yyyy-mm-dd hh:ii',
                'pickerPosition': 'bottom-left',
            }),
        }

    def __init__(self, *args, **kwargs):
        super(AircraftLogEntryForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('aircraft'),

            Div(
                Div(Field('entry_time'), css_class="col-md-4"),
                css_class="row",
            ),

            Div(
                Div(Field('event'), css_class="col-md-4"),
                Div(Field('pilot'), css_class="col-md-4"),
                css_class="row"
            ),
            Div(
                Div(Field('maintenance_rule'), css_class="col-md-4"),
                css_class="row"
            ),
            Div(
                Div(Field('comments'), css_class="col-md-8"),
                css_class="row"
            )
        )

    def clean(self):
        cleaned_data = super(AircraftLogEntryForm, self).clean()
        if cleaned_data['event'] and cleaned_data['maintenance_rule']:
            raise ValidationError({'maintenance_rule': _("You have to choose if it's maintenance log or flight log")})
        if not cleaned_data['event'] and not cleaned_data['maintenance_rule']:
            raise ValidationError(_("You have to select `maintenance rule` or `event`"))
        if cleaned_data['event'] and not cleaned_data['pilot']:
            raise ValidationError({'pilot': _("You have to select pilot if it's flight event")})


class AircraftMeterTypeForm(forms.ModelForm):
    aircraft = forms.ModelChoiceField(widget=forms.HiddenInput, queryset=Aircraft.objects.all())

    class Meta:
        model = AircraftMeterType
        fields = ('aircraft', 'name',)

    def __init__(self, *args, **kwargs):
        super(AircraftMeterTypeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('aircraft'),
            Div(
                Div(Field('name'), css_class="col-md-6"),
                css_class="row",
            ),
        )
