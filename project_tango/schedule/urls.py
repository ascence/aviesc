from django.conf.urls import include, url

from .views import CalendarList, CalendarDetail, EventsAPIView, AircraftCalendarEventUpdate, \
    AircraftCalendarEventCreate, AircraftCalendarEventDelete

urlpatterns = [
    url(r'^calendar/$', CalendarList.as_view(), name='calendar-list'),
    url(r'^calendar/(?P<pk>[\w]+)/$', CalendarDetail.as_view(), name="calendar-detail"),
    url(r'^calendar/(?P<pk>[\w]+)/events/create/$', AircraftCalendarEventCreate.as_view(), name="event-create"),
    url(r'^calendar/events/(?P<pk>[\w]+)/update/$', AircraftCalendarEventUpdate.as_view(), name="event-update"),
    url(r'^calendar/events/(?P<pk>[\w]+)/delete/$', AircraftCalendarEventDelete.as_view(), name="event-delete"),

    # api
    url(r'^api/calendar/(?P<pk>[\w]+)/events/$', EventsAPIView.as_view(), name="calendar-events"),
]
