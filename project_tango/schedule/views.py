from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView
from django.utils.decorators import method_decorator
from django.http import Http404
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save

from datetime import datetime, timedelta

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Calendar, AircraftEvent, Rule, EstimatedMeterValue, aircraft_event_post_save
from .serializers import EventSerializer
from .forms import AircraftEventForm, RuleForm, DateRangeForm
from project_tango.aircraft.models import AircraftMeterType


class CalendarList(ListView):
    model = Calendar
    template_name = 'dashboard/schedule/calendar_list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CalendarList, self).dispatch(request)

    def get_queryset(self):
        return Calendar.objects.filter(aircraft__company_account=self.request.user.company_account)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class CalendarDetail(DetailView):
    model = Calendar
    template_name = 'dashboard/schedule/calendar_detail.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CalendarDetail, self).dispatch(request)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class AircraftCalendarEventCreate(CreateView):
    model = AircraftEvent
    template_name = 'dashboard/schedule/event_create.html'
    form_class = AircraftEventForm
    repeat_form_class = RuleForm
    success_url = ''
    success_message = _("Event was created successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AircraftCalendarEventCreate, self).dispatch(request)

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        return super(AircraftCalendarEventCreate, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('calendar-detail', args=[self.object.calendar.pk])

    def get_form_kwargs(self):
        kwargs = super(AircraftCalendarEventCreate, self).get_form_kwargs()
        try:
            kwargs.update({'initial': {'calendar': Calendar.objects.get(pk=self.kwargs['pk'])}})
        except Calendar.DoesNotExist:
            pass
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['calendar_pk'] = self.kwargs['pk']
        if 'repeat_form' not in context:
            context['repeat_form'] = self.repeat_form_class()
        calendar = Calendar.objects.get(pk=self.kwargs['pk'])
        context['custom_parameters'] = calendar.aircraft.meter_types.all()
        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.form_class(request.POST)
        repeat_form = self.repeat_form_class(request.POST)
        if form.is_valid() and repeat_form.is_valid():
            for (k, val) in request.POST.items():
                if k.startswith('param') and not val:
                    messages.error(self.request, _('Please, fill all parameters'))
                    return self.render_to_response(self.get_context_data(form=form))
            post_save.disconnect(aircraft_event_post_save, sender=AircraftEvent)
            event = form.save()
            rule = Rule.objects.create(**repeat_form.cleaned_data)
            event.rule = rule
            for (k, val) in request.POST.items():
                if k.startswith('param'):
                    if val:
                        param_id = k.split('_')[1]
                        try:
                            param = AircraftMeterType.objects.get(id=param_id)
                            a = EstimatedMeterValue.objects.create(event=event, meter_type=param, value=val)
                        except (AttributeError, ValueError, AircraftMeterType.DoesNotExist):
                            pass
            post_save.connect(aircraft_event_post_save, sender=AircraftEvent)
            event.save()
            self.object = event
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, repeat_form=repeat_form))


class AircraftCalendarEventUpdate(UpdateView):
    model = AircraftEvent
    template_name = 'dashboard/schedule/event_update.html'
    form_class = AircraftEventForm
    repeat_form_class = RuleForm
    success_url = ''
    success_message = _("Event was updated successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AircraftCalendarEventUpdate, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('calendar-detail', args=[self.object.calendar.pk])

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        return super(AircraftCalendarEventUpdate, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(AircraftCalendarEventUpdate, self).get_form_kwargs()
        kwargs.update({'initial': {'calendar': self.object.calendar.pk}})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'repeat_form' not in context:
            context['repeat_form'] = self.repeat_form_class(
                instance=context['aircraftevent'].rule,
                initial={'end_how': 'until' if context['aircraftevent'].rule.until else 'times'} if context['aircraftevent'].rule else {'end_how': 'times'}
            )
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(request.POST, instance=self.object)
        repeat_form = self.repeat_form_class(request.POST)
        if form.is_valid() and repeat_form.is_valid():
            for (k, val) in request.POST.items():
                if k.startswith('param') and not val:
                    messages.error(self.request, _('Please, fill all parameters'))
                    return self.render_to_response(self.get_context_data(form=form))
            for (k, val) in request.POST.items():
                if k.startswith('param'):
                    if val:
                        param_id = k.split('_')[1]
                        try:
                            param = EstimatedMeterValue.objects.get(id=param_id)
                            param.value = val
                            param.save()
                        except (AttributeError, ValueError, EstimatedMeterValue.DoesNotExist):
                            pass
            event = form.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, repeat_form=repeat_form))


class AircraftCalendarEventDelete(DeleteView):
    model = AircraftEvent
    success_url = ''
    success_message = _("Event was deleted successfully")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AircraftCalendarEventDelete, self).dispatch(request)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('calendar-detail', args=[self.object.calendar.pk])

# API Views


class EventsAPIView(APIView):
    def get_object(self, pk):
        try:
            return Calendar.objects.get(pk=pk)
        except Calendar.DoesNotExist:
            raise Http404

    def get(self, request, pk=None):
        form = DateRangeForm(request.GET)
        if form.is_valid():
            all_events = self.get_object(pk=pk).events.all()
            data = EventSerializer(instance=all_events, many=True).data
            return Response(data, status=status.HTTP_200_OK)
        else:
            return Response([], status=status.HTTP_200_OK)

