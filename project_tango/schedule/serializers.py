from rest_framework import serializers

from .models import AircraftEvent


class EventSerializer(serializers.ModelSerializer):
    url = serializers.URLField(source='get_absolute_url')

    dates = serializers.SerializerMethodField()

    class Meta:
        model = AircraftEvent

    def get_dates(self, obj):
        try:
            return obj.rule.dates()
        except AttributeError:
            return []
