from django.contrib import admin

from .models import Calendar, AircraftEvent, Rule

admin.site.register([Calendar, AircraftEvent, Rule])
