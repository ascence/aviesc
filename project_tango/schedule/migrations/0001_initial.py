# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('aircraft', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AircraftEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_ip', models.CharField(editable=False, max_length=45)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_ip', models.CharField(editable=False, max_length=45)),
                ('start', models.DateTimeField(verbose_name='start')),
                ('end', models.DateTimeField(verbose_name='end')),
                ('title', models.CharField(verbose_name='title', max_length=255)),
                ('description', models.TextField(verbose_name='description', blank=True, null=True)),
                ('confirmed', models.BooleanField(editable=False, default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Calendar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('aircraft', models.OneToOneField(to='aircraft.Aircraft')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Rule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('freq', models.IntegerField(choices=[(-1, 'No repeat'), (3, 'Daily'), (2, 'Weekly'), (1, 'Monthly'), (0, 'Yearly')], default=3)),
                ('count', models.PositiveIntegerField(verbose_name='End after', blank=True, null=True, default=1)),
                ('until', models.DateTimeField(verbose_name='End by', blank=True, null=True)),
                ('interval', models.PositiveIntegerField(verbose_name='Repeat every', blank=True, null=True, default=1)),
                ('byweekday', django.contrib.postgres.fields.ArrayField(verbose_name='Repeat on', blank=True, null=True, size=None, base_field=models.CharField(max_length=64))),
                ('bymonthday', models.PositiveIntegerField(verbose_name='Repeat every day of month', blank=True, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='aircraftevent',
            name='calendar',
            field=models.ForeignKey(to='schedule.Calendar', related_name='events'),
        ),
        migrations.AddField(
            model_name='aircraftevent',
            name='created_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='+', editable=False),
        ),
        migrations.AddField(
            model_name='aircraftevent',
            name='maintenance',
            field=models.OneToOneField(to='aircraft.AircraftMaintenanceRule', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='aircraftevent',
            name='modified_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='+', editable=False),
        ),
        migrations.AddField(
            model_name='aircraftevent',
            name='rule',
            field=models.OneToOneField(to='schedule.Rule', blank=True, null=True),
        ),
    ]
