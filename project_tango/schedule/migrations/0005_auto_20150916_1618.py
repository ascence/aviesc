# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0004_auto_20150916_1209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aircraftevent',
            name='maintenance',
            field=models.ForeignKey(related_name='event_maintenances', null=True, to='aircraft.AircraftMaintenanceRule', blank=True, help_text='If event is for maintenance then select one.'),
        ),
    ]
