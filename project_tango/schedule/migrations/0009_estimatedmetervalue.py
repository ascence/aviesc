# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aircraft', '0005_auto_20150829_1554'),
        ('schedule', '0008_auto_20150917_1608'),
    ]

    operations = [
        migrations.CreateModel(
            name='EstimatedMeterValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('value', models.FloatField()),
                ('event', models.ForeignKey(to='schedule.AircraftEvent', related_name='custom_parameters')),
                ('meter_type', models.ForeignKey(to='aircraft.AircraftMeterType')),
            ],
            options={
                'ordering': ('meter_type__name',),
            },
        ),
    ]
