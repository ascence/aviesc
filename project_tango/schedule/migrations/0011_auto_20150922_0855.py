# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0010_aircraftevent_is_maintenance'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='aircraftevent',
            options={'ordering': ('start',)},
        ),
    ]
