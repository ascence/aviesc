# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0007_auto_20150916_1627'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aircraftevent',
            name='maintenance',
            field=models.OneToOneField(blank=True, to='aircraft.AircraftMaintenanceRule', help_text='If event is for maintenance then select one.', null=True),
        ),
    ]
