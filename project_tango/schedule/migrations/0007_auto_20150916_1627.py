# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0006_auto_20150916_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aircraftevent',
            name='confirmed',
            field=models.BooleanField(editable=False, default=True),
        ),
    ]
