# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0003_auto_20150829_1554'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aircraftevent',
            name='maintenance',
            field=models.OneToOneField(help_text='If event is for maintenance then select one.', related_name='event_maintenances', null=True, blank=True, to='aircraft.AircraftMaintenanceRule'),
        ),
        migrations.AlterField(
            model_name='rule',
            name='freq',
            field=models.IntegerField(default=-1, choices=[(-1, 'No repeat'), (3, 'Daily'), (2, 'Weekly'), (1, 'Monthly'), (0, 'Yearly')]),
        ),
    ]
