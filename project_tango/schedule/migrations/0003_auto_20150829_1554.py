# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0002_aircraftevent_pilot'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aircraftevent',
            name='maintenance',
            field=models.OneToOneField(to='aircraft.AircraftMaintenanceRule', blank=True, help_text='If event is for maintenance then select one.', null=True),
        ),
    ]
