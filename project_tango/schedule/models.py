from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.contrib.postgres.fields import ArrayField
from django.conf import settings
from django.utils import timezone
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from datetime import timedelta
from dateutil.rrule import *
from collections import namedtuple

from project_tango.core.models import AuditableModel, Entity


class Calendar(Entity):
    aircraft = models.OneToOneField('aircraft.Aircraft')

    @staticmethod
    def are_dates_overlap(start1, end1, start2, end2):
        """
        Return True if date1 and date2 overlap. Need to find another date
        :param start1 (datetime):
        :param end1 (datetime):
        :param start2 (datetime):
        :param end2 (datetime):
        :return:
        """
        Range = namedtuple('Range', ['start', 'end'])
        r1 = Range(start=start1, end=end1)
        r2 = Range(start=start2, end=end2)
        latest_start = max(r1.start, r2.start)
        earliest_end = min(r1.end, r2.end)
        overlap = (earliest_end - latest_start).days + 1
        return overlap != 0

    def check_or_find_slot(self, start, end, maintenance_event=None, days_backward=5):
        if days_backward < 0:
            return None
        max_time = self.aircraft.company_account.maint_work_end_hr
        base_start = start
        base_end = end
        range_events = self.events.filter(start__range=(start.date(), start.date() + timedelta(days=1)))
        if maintenance_event:
            range_events = range_events.exclude(pk=maintenance_event.pk)
        for event in range_events:
            # check if events overlap
            while Calendar.are_dates_overlap(start, end, event.start, event.end):
                start = start + timedelta(minutes=30)
                end = end + timedelta(minutes=30)
                if end.time() >= max_time:
                    # check previous day
                    duration = (base_end - base_start).seconds / 3600
                    prev_start = base_start.replace(
                        hour=self.aircraft.company_account.maint_work_start_hr.hour,
                        minute=self.aircraft.company_account.maint_work_start_hr.minute,
                        second=0
                    ) - timedelta(days=1)
                    prev_end = prev_start + timedelta(hours=duration)
                    return self.check_or_find_slot(
                        prev_start, prev_end, maintenance_event=maintenance_event, days_backward=days_backward - 1)
        return {'start': start, 'end': end}


class Event(AuditableModel):
    calendar = models.ForeignKey(Calendar, related_name='events')
    start = models.DateTimeField(_("start"))
    end = models.DateTimeField(_("end"))
    title = models.CharField(_("title"), max_length=255)
    description = models.TextField(_("description"), null=True, blank=True)
    # If this is a maintenance Event, then it will be created non-confirmed
    # and the user is required to confirm this appointment
    confirmed = models.BooleanField(editable=False, default=True)
    rule = models.OneToOneField('Rule', null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return "{} [{}]".format(self.title, self.calendar)

    @property
    def duration_hrs(self):
        return (self.end - self.start).seconds / 3600


class AircraftEvent(Event):
    pilot = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name="pilot_aircraft_events")
    maintenance = models.OneToOneField('aircraft.AircraftMaintenanceRule', null=True, blank=True,
                                       help_text=_("If event is for maintenance then select one."))
    is_maintenance = models.BooleanField(default=False)

    class Meta:
        ordering = ('start',)

    @property
    def get_absolute_url(self):
        return reverse('event-update', args=[str(self.id)])


class EstimatedMeterValue(models.Model):
    event = models.ForeignKey(AircraftEvent, related_name='custom_parameters')
    meter_type = models.ForeignKey('aircraft.AircraftMeterType')
    value = models.FloatField()

    class Meta:
        ordering = ('meter_type__name',)


class Rule(models.Model):
    FREQ = (
        (-1, 'No repeat'),
        (DAILY, _('Daily')),
        (WEEKLY, _('Weekly')),
        (MONTHLY, _('Monthly')),
        (YEARLY, _('Yearly')),
    )
    REPEAT_ON_LOOKUP = {
        '0': MO,
        '1': TU,
        '2': WE,
        '3': TH,
        '4': FR,
        '5': SA,
        '6': SU
    }
    freq = models.IntegerField(choices=FREQ, default=-1)
    count = models.PositiveIntegerField(null=True, blank=True, default=1, verbose_name=_('End after'))
    until = models.DateTimeField(blank=True, null=True, verbose_name=_('End by'))
    interval = models.PositiveIntegerField(null=True, blank=True, default=1, verbose_name=_('Repeat every'))
    byweekday = ArrayField(models.CharField(max_length=64), null=True, blank=True, verbose_name=_('Repeat on'))
    bymonthday = models.PositiveIntegerField(null=True, blank=True, verbose_name=_('Repeat every day of month'))

    def __str__(self):
        if hasattr(self, 'aircraftevent'):
            return '{}'.format(self.aircraftevent)
        return ''

    def dates(self, until=None, start=None):
        if self.freq != -1:
            _until = until if until else self.until
            _start = start if start else self.aircraftevent.start
            byweekday = []
            if self.byweekday:
                for item in self.byweekday:
                    byweekday.append(self.REPEAT_ON_LOOKUP[item])
            return list(rrule(
                freq=self.freq,
                count=self.count,
                until=_until,
                interval=self.interval,
                byweekday=tuple(byweekday) if byweekday else None,
                bymonthday=self.bymonthday,
                dtstart=_start,
            ))
        else:
            if hasattr(self, 'aircraftevent'):
                return [self.aircraftevent.start, ]
            else:
                return [start, ]


# Signals ##############################################################################################################


@receiver(post_save, sender=AircraftEvent)
def aircraft_event_post_save(sender, instance, **kwargs):
    # after create or update Aircraft Event check maintenance forecast
    if instance.maintenance is None:
        instance.calendar.aircraft.check_maintenances()


@receiver(post_delete, sender=AircraftEvent)
def aircraft_event_post_delete(sender, instance, **kwargs):
    # after delete Aircraft Event check maintenance forecast
    if instance.maintenance is None:
        instance.calendar.aircraft.check_maintenances()
