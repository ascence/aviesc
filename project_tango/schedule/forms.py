from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from dateutil.rrule import *

from datetimewidget.widgets import DateTimeWidget

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, HTML, Fieldset
from crispy_forms.bootstrap import Div, FormActions, AppendedText, PrependedText, InlineRadios, InlineCheckboxes

from .models import AircraftEvent, Calendar, Rule


class AircraftEventForm(forms.ModelForm):
    calendar = forms.ModelChoiceField(widget=forms.HiddenInput, queryset=Calendar.objects.all())

    class Meta:
        model = AircraftEvent
        exclude = ('rule',)
        widgets = {
            'description': forms.Textarea(attrs={'rows': 2}),
            'start': DateTimeWidget(bootstrap_version=3, options={
                'minuteStep': 15,
                'format': 'yyyy-mm-dd hh:ii',
                'pickerPosition': 'bottom-left',
            }),
            'end': DateTimeWidget(bootstrap_version=3, options={
                'minuteStep': 15,
                'format': 'yyyy-mm-dd hh:ii',
                'pickerPosition': 'bottom-left',
            })
        }

    def __init__(self, *args, **kwargs):
        super(AircraftEventForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('calendar'),
            Div(
                Div(Field('title'), css_class='col-md-12'),
                css_class="row",
            ),
            Div(
                Div(Field('start'), css_class='col-md-6'),
                Div(Field('end'), css_class='col-md-6'),
                css_class="row",
            ),
            Div(
                Div(Field('description'), css_class='col-md-12'),
                css_class="row",
            ),
            Div(
                css_class="row",
            ),
            Div(
                Div(Field('maintenance'), css_class='col-md-6'),
                css_class="row",
            ),
        )

    def clean(self):
        cleaned_data = super(AircraftEventForm, self).clean()

        if ('start' or 'end') not in cleaned_data:
            return

        if cleaned_data['start'] > cleaned_data['end']:
            raise ValidationError({'end': _('The end time must be later than the start time.')})


class DateRangeForm(forms.Form):
    start = forms.DateField()
    end = forms.DateField()


class RuleForm(forms.ModelForm):
    END_HOW = (
        ('times', 'End after X times'),
        ('until', 'End until date')
    )
    REPEAT_ON = (
        ('0', 'Mon'),
        ('1', 'Tue'),
        ('2', 'Wed'),
        ('3', 'Thu'),
        ('4', 'Fri'),
        ('5', 'Sat'),
        ('6', 'Sun'),
    )

    end_how = forms.ChoiceField(choices=END_HOW, initial='times', required=False, widget=forms.RadioSelect,
                                label=_('When stop recurring'))

    byweekday = forms.MultipleChoiceField(choices=REPEAT_ON, required=False, widget=forms.CheckboxSelectMultiple,
                                          label=_('Repeat on'))
    bymonthday = forms.IntegerField(required=False, min_value=1, max_value=31, label=_('Repeat on'))
    every_n_month = forms.IntegerField(min_value=1, initial=1, required=False, label=_('of every'))

    class Meta:
        model = Rule
        fields = ('freq', 'count', 'until', 'interval', 'byweekday', 'bymonthday', 'every_n_month', 'end_how')
        widgets = {
            'until': DateTimeWidget(bootstrap_version=3, options={
                'minuteStep': 15,
                'format': 'yyyy-mm-dd HH:ii',
                'pickerPosition': 'bottom-left',
            }),
        }

    def __init__(self, *args, **kwargs):
        super(RuleForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset(
                'Recurring options',
                Div(
                    Div(InlineRadios('freq'), css_class='col-md-12'),
                    css_class="row",
                ),
                Div(
                    Div(
                        Div(
                            AppendedText('interval', _('day(s)')),
                            css_class='col-md-4'
                        ),
                        css_id="daily_repeat_every",
                        css_class="row"
                    ),
                    Div(
                        Div(
                            InlineCheckboxes('byweekday'),
                            css_id="repeat_on",
                            css_class='col-md-12'
                        ),
                        css_class="row"
                    ),
                    Div(
                        Div(
                            AppendedText('bymonthday', _('th day')),
                            css_id='bymonthday',
                            css_class='col-md-4'
                        ),
                        css_class="row"
                    ),
                    Div(
                        Div(
                            AppendedText('every_n_month', _('month(s)')),
                            css_id='every_n_month',
                            css_class='col-md-4'
                        ),
                        css_class="row"
                    ),
                    Div(
                        Div(
                            InlineRadios('end_how'),
                            css_class='col-md-6'
                        ),
                        css_class="row"
                    ),
                    Div(
                        Div(
                            AppendedText('count', _('times')),
                            css_id='end_after',
                            css_class='col-md-4'
                        ),
                        css_class="row"
                    ),
                    Div(
                        Div(
                            PrependedText('until', _('until')),
                            css_id='end_until',
                            css_class='col-md-4'
                        ),
                        css_class="row"
                    ),
                    css_id="recurring",
                    css_class='col-md-8'
                ),
                css_class="hidden"  # temporary disabled
            )
        )

    def clean(self):
        data = super(RuleForm, self).clean()

        if data['freq'] in [DAILY, WEEKLY] and not data['interval']:
            raise ValidationError({'interval': _('This field is required.')})

        if data['end_how'] == 'times' and not data['count']:
            raise ValidationError({'count': _('This field is required.')})

        if data['end_how'] == 'until' and not data['until']:
            raise ValidationError({'until': _('This field is required.')})

        if data['freq'] == WEEKLY and not data['byweekday']:
            raise ValidationError({'byweekday': _('This field is required.')})

        if data['freq'] == DAILY:
            data['byweekday'] = None
            data['bymonthday'] = None

        if data['freq'] == WEEKLY:
            data['bymonthday'] = None

        if data['freq'] == MONTHLY:
            if not data['bymonthday']:
                raise ValidationError({'bymonthday': _('This field is required.')})
            if not data['every_n_month']:
                raise ValidationError({'every_n_month': _('This field is required.')})
            data['byweekday'] = None
            data['interval'] = data['every_n_month']

        if data['end_how'] == 'times':
            data['until'] = None
        if data['end_how'] == 'until':
            data['count'] = None

        del data['end_how']
        del data['every_n_month']

        return data
