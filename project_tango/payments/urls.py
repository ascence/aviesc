from django.conf.urls import include, url

from .views import pay_for_subscription, StripeWebhook

urlpatterns = [
    url(r'^subscription/+(?P<sub_pk>[\w]+)?$', pay_for_subscription, name='pay-for-subscription'),
    # url(r'^subscription/(?P<sub_pk>[\w]+)/$', pay_for_subscription, name='pay-for-subscription'),
    url(r'^stripe/webhook/$', StripeWebhook.as_view(),)
]
