# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('description', models.CharField(max_length=512)),
                ('date_succeeded', models.DateTimeField(default=django.utils.timezone.now)),
                ('stripe_charge_id', models.CharField(max_length=256)),
                ('amount', models.PositiveIntegerField(help_text='in cents')),
                ('company_account', models.ForeignKey(to='core.CompanyAccount', related_name='payments')),
                ('subscription', models.ForeignKey(to='core.Subscription', null=True, blank=True)),
            ],
        ),
    ]
