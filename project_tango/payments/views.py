from django.shortcuts import render_to_response, RequestContext, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

import stripe
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .forms import StripeForm
from .models import Payment
from project_tango.core.models import Subscription

stripe.api_key = 'sk_test_GROLVE3nPQk70GKPN1IO9eqM'


class StripeWebhook(APIView):
    @csrf_exempt
    def post(self, request):
        """
        Get event from stripe. Not required for charge. Charges are reported synchronously.
        """
        event_id = request.data['id']
        event = stripe.Event.retrieve(event_id)

        if event.type == 'charge.succeeded':
            pass
        elif event.type == 'charge.failed':
            pass

        return Response(status=status.HTTP_200_OK)


@login_required()
def pay_for_subscription(request, sub_pk=None):
    form = StripeForm()
    if not sub_pk:  # if change subscription
        subscription = request.user.company_account.subscription
    else:  # if pay for extend current subscription
        subscription = get_object_or_404(Subscription, pk=sub_pk)
    if request.POST:
        token = request.POST['stripeToken']
        description = "Subscription: {}, Company: {}".format(subscription, request.user.company_account)
        try:
            charge = stripe.Charge.create(
                amount=int(subscription.price * 100),  # amount in cents
                currency="usd",
                source=token,
                description=description
            )
            Payment.objects.create(
                company_account=request.user.company_account,
                description=description,
                amount=int(subscription.price * 100),
                stripe_charge_id=charge['id'],
                subscription=subscription,
            )
            request.user.company_account.extend_subscription_valid_until(period=subscription.period)
            if sub_pk:  # if change subscription
                request.user.company_account.subscription = subscription
                request.user.company_account.save()
            messages.success(request,
                             _('Your payment for subscription {} has been accepted. Thanks.'.format(subscription)))

            return HttpResponseRedirect(reverse('dashboard'))
        except stripe.error.CardError as e:
            messages.error(request, _('Sorry. Your card has been declined. Try again.'.format(subscription)))

    return render_to_response('dashboard/payments/pay_for_subscription.html',
                              {
                                  'form': form,
                                  'subscription': subscription,
                              }, context_instance=RequestContext(request))

