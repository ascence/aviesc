from django import forms
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, HTML
from crispy_forms.bootstrap import Div, FormActions


class StripeForm(forms.Form):
    card_number = forms.CharField(max_length=20, label=_('Card Number'), widget=forms.TextInput(
        attrs={'placeholder': 'XXXX-XXXX-XXXX-XXXX', 'data-stripe': 'number'}
    ))
    cvc = forms.CharField(max_length=4, label=_('CVC'), widget=forms.TextInput(
        attrs={'data-stripe': 'cvc'}
    ))
    expiration_mm = forms.CharField(max_length=2, label='Expiration MM', widget=forms.TextInput(
        attrs={'placeholder': 'MM', 'data-stripe': 'exp-month'}
    ))
    expiration_yy = forms.CharField(max_length=4, label='Expiration YYYY', widget=forms.TextInput(
        attrs={'placeholder': 'YYYY', 'data-stripe': 'exp-year'}
    ))

    def __init__(self, *args, **kwargs):
        super(StripeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(Field('card_number'), css_class="col-md-4"),
                css_class="row"
            ),
            # Div(
            #     HTML('<div class="col-md-4"><label></label></div>'),
            #     css_class="row"
            # ),
            Div(
                Div(Field('expiration_mm'), css_class="col-md-2"),
                Div(Field('expiration_yy'), css_class="col-md-2"),
                Div(Field('cvc'), css_class="col-md-2"),
                css_class="row"
            ),
            Div(

                css_class="row"
            ),
        )
