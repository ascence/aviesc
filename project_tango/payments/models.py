from django.db import models
from django.utils import timezone

from dateutil.relativedelta import relativedelta

from project_tango.core.models import CompanyAccount, Subscription


class Payment(models.Model):
    company_account = models.ForeignKey(CompanyAccount, related_name='payments')
    description = models.CharField(max_length=512)

    date_succeeded = models.DateTimeField(default=timezone.now)
    stripe_charge_id = models.CharField(max_length=256)
    amount = models.PositiveIntegerField(help_text='in cents')  # amount in cents

    subscription = models.ForeignKey(Subscription, null=True, blank=True)
