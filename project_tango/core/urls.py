from django.conf.urls import include, url

from .views import user_profile, home, dashboard, CompanyAccountUpdate, CustomSignupView, CompanyUserList, \
    CompanyUserCreate, CompanyUserDelete, CompanyUserUpdate, change_subscription, change_subscription_confirm

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^dashboard/$', dashboard, name='dashboard'),
    url(r'^company_settings/$', CompanyAccountUpdate.as_view(), name='company-settings'),
    url(r'^user/profile/$', user_profile, name='user-profile'),
    url(r'^user/signup/$', CustomSignupView.as_view(), name='account_signup'),
    url(r'^users/$', CompanyUserList.as_view(), name="company-users-list"),
    url(r'^users/create/$', CompanyUserCreate.as_view(), name="company-user-create"),
    url(r'^users/(?P<pk>[\w]+)/delete/$', CompanyUserDelete.as_view(), name="company-user-delete"),
    url(r'^users/(?P<pk>[\w]+)/update/$', CompanyUserUpdate.as_view(), name="company-user-update"),
    url(r'^change_subscription/$', change_subscription, name="change-subscription"),
    url(r'^change_subscription/(?P<pk>[\w]+)/confirm/$', change_subscription_confirm,
        name="change-subscription-confirm"),
]
