from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin, Group
)
from django.conf import settings
from django.utils.text import slugify
from django.core.files import File

from datetime import time
from dateutil.relativedelta import relativedelta
from PIL import Image
from crequest.middleware import CrequestMiddleware
from io import BytesIO


class BaseModel(models.Model):
    # for future development...

    class Meta:
        abstract = True


class Entity(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class UserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password, company_account=None):
        """
        Creates and saves a User with the given email, first_name, last_name and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            company_account=company_account,
        )
        user.set_password(password)
        user.save(using=self._db)

        UserDetails.objects.create(user=user)

        return user

    def create_superuser(self, email, first_name, last_name, password):
        """
        Creates and saves a superuser with the given email, first_name, last_name and password.
        """
        user = self.create_user(
            email,
            password=password,
            first_name=first_name,
            last_name=last_name
        )
        user.is_admin = True
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(PermissionsMixin, AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)

    company_account = models.ForeignKey('CompanyAccount', null=True, blank=True, related_name='users')
    company_admin = models.ForeignKey('CompanyAccount', null=True, blank=True, related_name='admins')
    is_active = models.BooleanField(default=True)  # required by allauth
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        # The user is identified by their email address
        return '{} {}'.format(self.first_name, self.last_name)

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):
        return self.get_full_name()

    def has_perm(self, perm, obj=None):
        """Does the user have a specific permission?"""
        return super(User, self).has_perm(perm, obj)

    def has_module_perms(self, app_label):
        """Does the user have permissions to view the app `app_label`?"""
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        """Is the user a member of staff?"""
        # Simplest possible answer: All admins are staff
        return self.is_admin


class UserDetails(models.Model):
    user = models.OneToOneField(User, related_name='user_details')
    is_pilot = models.BooleanField(default=False)


def _time_seven():
    return time(7, 0)


def _time_five():
    return time(17, 0)


def upload_company_logo_to(instance, filename):
    ext = filename.split('.')
    path = 'companies/{0}_{1}/logo.{2}'.format(slugify(instance.company_name), instance.pk, ext[-1])
    print(path)
    return path


class CompanyAccount(models.Model):
    company_name = models.CharField(max_length=255, blank=True)
    company_logo = models.ImageField(upload_to=upload_company_logo_to, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    subscription = models.ForeignKey('Subscription', null=True, blank=True)
    subscription_valid_until = models.DateTimeField(null=True, blank=True)

    # settings for the Maintenance Forecaster
    maint_work_start_hr = models.TimeField(default=_time_seven, verbose_name=_('Maintenance Start Hour'))
    maint_work_end_hr = models.TimeField(default=_time_five, verbose_name=_('Maintenance End Hour'))
    maint_work_wkend = models.BooleanField(default=False, verbose_name=_('Maintenance  Hour'))
    forecast_ahead_days = models.IntegerField(blank=False, null=False, default=90)

    # address
    address = models.CharField(max_length=250, blank=True)
    city = models.CharField(max_length=250, blank=True)
    zip_code = models.CharField(max_length=16, blank=True)
    country = models.CharField(max_length=250, blank=True)

    def __str__(self):
        return "{} ({})".format(self.company_name, "Active" if self.is_active else "Not active")

    def extend_subscription_valid_until(self, period):
        self.subscription_valid_until = timezone.now() + relativedelta(months=+period)
        self.save()

    def is_subscription_valid(self):
        if not self.subscription:
            return False
        if self.subscription.price == 0:
            return True
        return self.subscription_valid_until >= timezone.now()

    def company_pilots(self):
        return self.users.filter(user_details__is_pilot=True)

    def save(self, *args, **kwargs):
        if self.company_logo:
            image = Image.open(self.company_logo)
            wpercent = (400 / float(image.size[0]))
            hsize = int((float(image.size[1]) * float(wpercent)))
            image = image.resize((400, hsize), Image.ANTIALIAS)
            output = BytesIO()
            image.save(output, 'JPEG', quality=95)
            self.company_logo.file = File(output, 'logo.jpg')
        super(CompanyAccount, self).save(*args, **kwargs)


class Subscription(models.Model):
    name = models.CharField(max_length=255)
    amount_of_users = models.PositiveIntegerField(default=1)
    amount_of_aircrafts = models.PositiveIntegerField(default=1)
    period = models.PositiveIntegerField(default=1)  # monthly
    price = models.FloatField(default=10)
    color = models.CharField(max_length=24, choices=(('green', 'Green'), ('red', 'Red'), ('blue', 'Blue')))

    def __str__(self):
        return "{} (${}/month)".format(self.name, int(self.price))

    def total_cost(self):
        return float(self.price * self.period)


class AuditableModel(BaseModel):
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, editable=False, related_name='+')
    created_ip = models.CharField(max_length=45, editable=False)
    modified_on = models.DateTimeField(auto_now=True, editable=False)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, editable=False, related_name='+')
    modified_ip = models.CharField(max_length=45, editable=False)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        request = CrequestMiddleware.get_request()
        if request:
            ip = request.META.get('HTTP_X_FORWARDED_FOR', None)
            if ip:
                ip = ip.split(', ')[0]
            else:
                ip = request.META.get('REMOTE_ADDR', 'unknown')
            if request.user and not self.pk:
                self.created_by = request.user
                self.created_on = timezone.now()
                self.created_ip = ip
            if request.user:
                self.modified_by = request.user
                self.modified_on = timezone.now()
                self.modified_ip = ip
        if settings.TESTING:
            test_user = User.objects.first()
            self.modified_by = test_user
            self.created_by = test_user
        super(AuditableModel, self).save(*args, **kwargs)
