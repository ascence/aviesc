from django.db.models import Q
from django.shortcuts import render_to_response, RequestContext, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from django.contrib import messages
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import permission_required
from django.utils import timezone

from allauth.account.views import SignupView
from dateutil.relativedelta import relativedelta
from datetime import timedelta


from .models import Subscription, User, CompanyAccount
from .forms import CompanyUserCreateForm, CompanyUserUpdateForm, CompanyUserDetailsForm, CompanyAccountForm
from .permissions import UserPermissions
from project_tango.payments.models import Payment
from project_tango.schedule.models import AircraftEvent
from project_tango.aircraft.models import AircraftMaintenanceRule


class CustomSignupView(SignupView):
    """ Override allauth signup view """

    def get_context_data(self, **kwargs):
        context = super(CustomSignupView, self).get_context_data(**kwargs)
        context['subscriptions'] = Subscription.objects.all().order_by('price')
        return context


def home(request):
    return render_to_response('home.html', {}, context_instance=RequestContext(request))


@login_required
def dashboard(request):
    company_account = request.user.company_account
    payments = Payment.objects.filter(company_account=company_account)
    upcoming_events = AircraftEvent.objects.filter(
        Q(start__gte=timezone.now()) & Q(end__lte=timezone.now()+timedelta(days=2)) &
        Q(calendar__aircraft__company_account=company_account))
    maintenances = AircraftMaintenanceRule.objects.filter(aircraft__company_account=company_account)
    close_maintenances = list()
    for maintenance in maintenances:
        result = maintenance.predict_maintenance()
        result['maintenance'] = maintenance
        close_maintenances.append(result)

    return render_to_response('dashboard/dashboard.html', {
        'company_account': company_account,
        'payments': payments,
        'upcoming_events': upcoming_events,
        'close_maintenances': close_maintenances,
    }, context_instance=RequestContext(request))


@login_required
def user_profile(request):
    return render_to_response('dashboard/profile.html', {}, context_instance=RequestContext(request))


@login_required
@permission_required('core.change_subscription', login_url='/dashboard/')
def change_subscription(request):
    subscriptions = Subscription.objects.all().order_by('price')
    current_subscription = request.user.company_account.subscription
    sub_id = request.GET.get('sub_id', None)
    if sub_id:
        subscription = get_object_or_404(Subscription, pk=sub_id)
        return render_to_response('dashboard/admin/change_subscription_confirm.html', {
            'new_subscription': subscription,
            'old_subscription': current_subscription,
        }, context_instance=RequestContext(request))
    return render_to_response('dashboard/admin/change_subscription.html', {
        'subscriptions': subscriptions,
        'current_subscription': current_subscription,
    }, context_instance=RequestContext(request))


@login_required
@permission_required('core.change_subscription', login_url='/dashboard/')
def change_subscription_confirm(request, pk):
    new_subscription = get_object_or_404(Subscription, pk=pk)
    current_subscription = request.user.company_account.subscription

    if new_subscription != current_subscription:
        if (new_subscription.amount_of_users < request.user.company_account.users.count()) \
                or (new_subscription.amount_of_aircrafts < request.user.company_account.aircrafts.count()):
            messages.error(request, _(
                "New subscription allow only for {} Staff/Users. Please remove them first.".format(
                    new_subscription.amount_of_users)))
            return HttpResponseRedirect(reverse('change-subscription'))

    if new_subscription.price <= current_subscription.price:
        request.user.company_account.subscription = new_subscription
        request.user.company_account.save()
        messages.success(request, _('Subscription has been changed to `{}`'.format(new_subscription)))
        return HttpResponseRedirect(reverse('dashboard'))
    else:
        return HttpResponseRedirect(reverse('pay-for-subscription', args=[new_subscription.id]))


class CompanyUserList(ListView):
    model = User
    template_name = 'dashboard/admin/users_list.html'

    def get_queryset(self):
        return User.objects.filter(company_account=self.request.user.company_account)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subscription'] = self.request.user.company_account.subscription
        context['amount_of_users'] = self.get_queryset().count()
        return context


class CompanyUserCreate(CreateView):
    model = User
    template_name = 'dashboard/admin/user_create.html'
    form_class = CompanyUserCreateForm
    success_url = reverse_lazy('company-users-list')
    success_message = _("User was created successfully")

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        del form.cleaned_data['password2']
        form.cleaned_data['company_account'] = self.request.user.company_account
        User.objects.create_user(**form.cleaned_data)
        return HttpResponseRedirect(self.success_url)

    def dispatch(self, request, *args, **kwargs):
        if not UserPermissions.has_subscription_user_limit(request.user):
            messages.error(self.request, _("Sorry, you've reached your limit! Extend your subscription."))
            return HttpResponseRedirect(self.success_url)
        return super(CompanyUserCreate, self).dispatch(request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class CompanyUserDelete(DeleteView):
    model = User
    success_url = reverse_lazy('company-users-list')
    success_message = _("User was deleted successfully")


class CompanyUserUpdate(UpdateView):
    model = User
    template_name = 'dashboard/admin/user_update.html'
    form_class = CompanyUserUpdateForm
    second_form_class = CompanyUserDetailsForm
    success_url = reverse_lazy('company-users-list')
    success_message = _("User was updated successfully")

    def get_context_data(self, **kwargs):
        context = super(CompanyUserUpdate, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(instance=self.get_object())
        if 'form2' not in context:
            context['form2'] = self.second_form_class(instance=self.get_object().user_details)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(request.POST, instance=self.get_object())
        second_form = self.second_form_class(request.POST, instance=self.get_object().user_details)
        if form.is_valid() and second_form.is_valid():
            user = form.save()
            if form.cleaned_data['is_company_admin'] is True:
                if user.company_admin is None:
                    user.company_admin = request.user.company_account
                    user.groups.add(Group.objects.get(name='company admin'))
                    user.save()
            else:
                if user.company_admin is not None and user != request.user:
                    user.company_admin = None
                    user.groups.clear()
                    user.save()
            second_form.save()
            messages.success(self.request, self.success_message)
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, form2=second_form))


class CompanyAccountUpdate(UpdateView):
    model = CompanyAccount
    template_name = 'dashboard/company_account_update.html'
    form_class = CompanyAccountForm
    success_url = reverse_lazy('dashboard')
    success_message = _("Company was updated successfully")

    def get_object(self, queryset=None):
        return self.request.user.company_account

    def form_valid(self, form):
        response = super(CompanyAccountUpdate, self).form_valid(form)
        messages.success(self.request, self.success_message)
        return response
