from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from datetimewidget.widgets import TimeWidget

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, HTML, Fieldset
from crispy_forms.bootstrap import Div, FormActions

from allauth.account.forms import LoginForm, ResetPasswordKeyForm, ResetPasswordForm

from .models import User, UserDetails, CompanyAccount


class CustomLoginForm(LoginForm):
    def __init__(self, *args, **kwargs):
        super(CustomLoginForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(Field('login'), css_class="col-md-6"),
                css_class="row"
            ),
            Div(
                Div(Field('password'), css_class="col-md-6"),
                css_class="row"
            ),
            FormActions(
                HTML('<a class="btn btn-link" href="{}">{}</a>'
                     .format(reverse('account_reset_password'), _("Forgot Password?"))),
                Submit('signin', _('Sign in &raquo;')),
            )
        )


class CustomResetPasswordForm(ResetPasswordForm):
    def __init__(self, *args, **kwargs):
        super(CustomResetPasswordForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(Field('email'), css_class="col-md-6"),
                css_class="row"
            ),
        )


class CustomResetPasswordKeyForm(ResetPasswordKeyForm):
    def __init__(self, *args, **kwargs):
        super(CustomResetPasswordKeyForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(Field('password1'), css_class="col-md-6"),
                css_class="row"
            ),
            Div(
                Div(Field('password2'), css_class="col-md-6"),
                css_class="row"
            ),
        )


class CompanyUserCreateForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput, label=_('Password'))
    password2 = forms.CharField(widget=forms.PasswordInput, label=_('Password (again)'))

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password', 'password2']

    def __init__(self, *args, **kwargs):
        super(CompanyUserCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(Field('email'), css_class="col-md-6"),
                css_class="row"
            ),
            Div(
                Div(Field('first_name'), css_class="col-md-6"),
                Div(Field('last_name'), css_class="col-md-6"),
                css_class="row"
            ),
            Div(
                Div(Field('password'), css_class="col-md-6"),
                Div(Field('password2'), css_class="col-md-6"),
                css_class="row"
            ),
        )

    def clean_password2(self):
        if "password" in self.cleaned_data and "password2" in self.cleaned_data:
            if self.cleaned_data["password"] != self.cleaned_data["password2"]:
                raise forms.ValidationError(_("You must type the same password each time."))
        return self.cleaned_data["password2"]


class CompanyUserUpdateForm(forms.ModelForm):
    is_company_admin = forms.BooleanField(label=_('is admin'), required=False)

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'is_company_admin']
        widgets = {
            'email': forms.EmailInput(attrs={'readonly': 'readonly'})
        }

    def __init__(self, *args, **kwargs):
        super(CompanyUserUpdateForm, self).__init__(*args, **kwargs)
        self.fields['is_company_admin'].initial = kwargs['instance'].company_admin is not None
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(Field('email'), css_class="col-md-6"),
                css_class="row"
            ),
            Div(
                Div(Field('first_name'), css_class="col-md-6"),
                Div(Field('last_name'), css_class="col-md-6"),
                css_class="row"
            ),
        )


class CompanyUserDetailsForm(forms.ModelForm):
    class Meta:
        model = UserDetails
        fields = ['is_pilot', ]

    def __init__(self, *args, **kwargs):
        super(CompanyUserDetailsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(Field('is_pilot'), css_class="col-md-6"),
                css_class="row"
            ),
        )


class CompanyAccountForm(forms.ModelForm):
    class Meta:
        model = CompanyAccount
        fields = ['company_logo', 'maint_work_start_hr', 'maint_work_end_hr', 'company_name', 'address', 'city',
                  'zip_code', 'country']
        widgets = {
            'maint_work_start_hr': TimeWidget(bootstrap_version=3, options={
                'minuteStep': 5,
                'pickerPosition': 'bottom-left',
            }),
            'maint_work_end_hr': TimeWidget(bootstrap_version=3, options={
                'minuteStep': 5,
                'pickerPosition': 'bottom-left',
            }),
            'company_logo': forms.FileInput,

        }

    def __init__(self, *args, **kwargs):
        super(CompanyAccountForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset(
                _('General'),
                Div(
                    Div(Field('company_name'), css_class="col-md-6"),
                    css_class="row"
                ),
                Div(
                    Div(Field('company_logo'), css_class="col-md-6"),
                    css_class="row"
                ),
                Div(
                    Div(
                        HTML("""{% if form.company_logo.value %}<img class="img-responsive"
                        src="{{ MEDIA_URL }}{{ form.company_logo.value }}"><br/>{% endif %}""", ),
                        css_class="col-md-6"),
                    css_class="row"
                )
            ),
            Fieldset(
                _('Address'),
                Div(
                    Div(Field('address'), css_class="col-md-6"),
                    Div(Field('zip_code'), css_class="col-md-3"),
                    Div(Field('city'), css_class="col-md-3"),
                    css_class="row",
                ),
                Div(
                    Div(Field('country'), css_class="col-md-4"),
                    css_class="row",
                )
            ),
            Fieldset(
                _('Maintenance Forecaster'),
                Div(
                    Div(Field('maint_work_start_hr'), css_class="col-md-3"),
                    Div(Field('maint_work_end_hr'), css_class="col-md-3"),
                    css_class="row"
                ),
            )
        )
