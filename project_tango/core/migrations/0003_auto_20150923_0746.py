# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import project_tango.core.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_companyaccount_maint_work_end_hr'),
    ]

    operations = [
        migrations.AddField(
            model_name='companyaccount',
            name='address',
            field=models.CharField(max_length=250, blank=True),
        ),
        migrations.AddField(
            model_name='companyaccount',
            name='city',
            field=models.CharField(max_length=250, blank=True),
        ),
        migrations.AddField(
            model_name='companyaccount',
            name='company_logo',
            field=models.ImageField(null=True, blank=True, upload_to=project_tango.core.models.upload_company_logo_to),
        ),
        migrations.AddField(
            model_name='companyaccount',
            name='country',
            field=models.CharField(max_length=250, blank=True),
        ),
        migrations.AddField(
            model_name='companyaccount',
            name='zip_code',
            field=models.CharField(max_length=16, blank=True),
        ),
    ]
