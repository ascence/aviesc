# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import project_tango.core.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='companyaccount',
            name='maint_work_end_hr',
            field=models.TimeField(default=project_tango.core.models._time_five, verbose_name='Maintenance End Hour'),
        ),
    ]
