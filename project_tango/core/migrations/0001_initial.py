# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import project_tango.core.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(null=True, blank=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, verbose_name='superuser status', help_text='Designates that this user has all permissions without explicitly assigning them.')),
                ('email', models.EmailField(verbose_name='email address', max_length=255, unique=True)),
                ('first_name', models.CharField(max_length=64)),
                ('last_name', models.CharField(max_length=64)),
                ('is_active', models.BooleanField(default=True)),
                ('is_admin', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CompanyAccount',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('company_name', models.CharField(blank=True, max_length=255)),
                ('is_active', models.BooleanField(default=True)),
                ('subscription_valid_until', models.DateTimeField(null=True, blank=True)),
                ('maint_work_start_hr', models.TimeField(default=project_tango.core.models._time_seven, verbose_name='Maintenance Start Hour')),
                ('maint_work_wkend', models.BooleanField(default=False, verbose_name='Maintenance  Hour')),
                ('forecast_ahead_days', models.IntegerField(default=90)),
            ],
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('amount_of_users', models.PositiveIntegerField(default=1)),
                ('amount_of_aircrafts', models.PositiveIntegerField(default=1)),
                ('period', models.PositiveIntegerField(default=1)),
                ('price', models.FloatField(default=10)),
                ('color', models.CharField(choices=[('green', 'Green'), ('red', 'Red'), ('blue', 'Blue')], max_length=24)),
            ],
        ),
        migrations.CreateModel(
            name='UserDetails',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('is_pilot', models.BooleanField(default=False)),
                ('user', models.OneToOneField(related_name='user_details', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='companyaccount',
            name='subscription',
            field=models.ForeignKey(blank=True, null=True, to='core.Subscription'),
        ),
        migrations.AddField(
            model_name='user',
            name='company_account',
            field=models.ForeignKey(blank=True, null=True, related_name='users', to='core.CompanyAccount'),
        ),
        migrations.AddField(
            model_name='user',
            name='company_admin',
            field=models.ForeignKey(blank=True, null=True, related_name='admins', to='core.CompanyAccount'),
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups', related_name='user_set', to='auth.Group'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions', related_name='user_set', to='auth.Permission'),
        ),
    ]
