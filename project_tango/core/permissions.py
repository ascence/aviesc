from .models import User
from project_tango.aircraft.models import Aircraft

class UserPermissions(object):
    @staticmethod
    def has_subscription_user_limit(user):
        """
        Check if company not reached limit of allowed users in subscription.
        :param user:
        """
        company_account = user.company_account
        return User.objects.filter(
            company_account=company_account).count() < company_account.subscription.amount_of_users

    @staticmethod
    def has_admin_permission(user):
        """
        Check if user is admin of his company account.
        :param user:
        """
        return user.company_account == user.company_admin

    @staticmethod
    def has_subscription_aircraft_limit(user):
        company_account = user.company_account
        return Aircraft.objects.filter(
            company_account=company_account).count() < company_account.subscription.amount_of_aircrafts
