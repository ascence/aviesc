from django.contrib import admin

from .models import User, UserDetails, CompanyAccount, Subscription
from project_tango.payments.models import Payment


class UserDetailsInline(admin.StackedInline):
    model = UserDetails


class UserAdmin(admin.ModelAdmin):
    inlines = [
        UserDetailsInline,
    ]


admin.site.register(User, UserAdmin)


class UserInline(admin.TabularInline):
    model = User
    fk_name = 'company_account'


class PaymentsInline(admin.TabularInline):
    model = Payment


class CompanyAccountAdmin(admin.ModelAdmin):
    inlines = [
        UserInline,
        PaymentsInline,
    ]


admin.site.register(CompanyAccount, CompanyAccountAdmin)

admin.site.register([Subscription, ])
