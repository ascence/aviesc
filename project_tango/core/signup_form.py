"""
In some reason Signup form can't be hold with rest allauth forms. Allauth raise circural dependency error then.
"""
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import Group
from django.utils import timezone

from datetime import timedelta

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field
from crispy_forms.bootstrap import Div

from .models import CompanyAccount, Subscription, UserDetails


class CustomSignupForm(forms.Form):
    company_name = forms.CharField(max_length=255, label=_('Company name'), required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _('Company name')}))
    first_name = forms.CharField(max_length=30, label=_('First name'),
                                 widget=forms.TextInput(attrs={'placeholder': _('First name')}))
    last_name = forms.CharField(max_length=30, label=_('Last name'),
                                widget=forms.TextInput(attrs={'placeholder': _('Last name')}))
    subscription = forms.CharField(widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        super(CustomSignupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(Field('email'), css_class="col-md-12"),
                css_class="row"
            ),
            Div(
                Div(Field('company_name'), css_class="col-md-4"),
                Div(Field('first_name'), css_class="col-md-4"),
                Div(Field('last_name'), css_class="col-md-4"),
                css_class="row"
            ),
            Div(
                Div(Field('password1'), css_class="col-md-4"),
                Div(Field('password2'), css_class="col-md-4"),
                css_class="row"
            ),
            Field('subscription'),
        )

    def signup(self, request, user):
        subscription_id = self.cleaned_data['subscription']
        try:
            subscription = Subscription.objects.get(id=subscription_id)
        except Subscription.DoesNotExist:
            subscription = Subscription.objects.first()

        company_name = self.cleaned_data['company_name']
        company = CompanyAccount.objects.create(
            company_name=company_name,
            subscription=subscription,
            subscription_valid_until=timezone.now() + timedelta(days=1)
        )
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.company_account = company
        user.company_admin = company
        user.groups.add(Group.objects.get(name='company admin'))
        user.save()
        UserDetails.objects.create(user=user)
