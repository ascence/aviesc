from rest_framework import serializers

from project_tango.aircraft.models import Aircraft


class AircraftDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Aircraft
