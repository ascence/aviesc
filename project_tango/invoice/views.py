from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib import messages
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from django.utils.translation import ugettext_lazy as _

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Invoice, Extras
from .forms import InvoiceCreateForm, ExtrasCreateForm, ExtrasUpdateForm
from .serializers import AircraftDetailsSerializer
from project_tango.aircraft.models import Aircraft


class InvoiceList(ListView):
    model = Invoice
    template_name = 'dashboard/invoice/invoice_list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(InvoiceList, self).dispatch(request)

    def get_queryset(self):
        return Invoice.objects.all()

    def get_context_data(self, **kwargs):
        context = super(InvoiceList, self).get_context_data(**kwargs)
        return context


class InvoiceCreate(CreateView):
    model = Invoice
    template_name = 'dashboard/invoice/invoice_create.html'
    form_class = InvoiceCreateForm
    extras_form_class = ExtrasCreateForm
    success_url = reverse_lazy('invoice-list')
    success_message = _("Invoice was created successfully")

    @method_decorator(login_required)
    @method_decorator(permission_required('invoice.add_invoice', login_url='/dashboard/'))
    def dispatch(self, request, *args, **kwargs):
        return super(InvoiceCreate, self).dispatch(request)

    def get_context_data(self, **kwargs):
        context = super(InvoiceCreate, self).get_context_data(**kwargs)
        context['company_account'] = self.request.user.company_account
        context['form'].fields['aircraft'].queryset = context['company_account'].aircrafts.all()
        context['form'].fields['pilot'].queryset = context['company_account'].company_pilots()
        if self.request.POST:
            context['extras_formset'] = inlineformset_factory(
                Invoice, Extras, form=self.extras_form_class, extra=0)(self.request.POST)
        else:
            context['extras_formset'] = inlineformset_factory(
                Invoice, Extras, form=self.extras_form_class, extra=1)()
        return context

    def get_form_kwargs(self):
        kwargs = super(InvoiceCreate, self).get_form_kwargs()
        kwargs.update({'initial': {'company_account': self.request.user.company_account.pk}})
        return kwargs

    def form_valid(self, form):
        context = self.get_context_data(form=form)
        formset = context['extras_formset']
        if formset.is_valid():
            self.object = form.save()
            formset.instance = self.object
            formset.save()
            messages.success(self.request, self.success_message)
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form))


class InvoiceUpdate(UpdateView):
    model = Invoice
    template_name = 'dashboard/invoice/invoice_update.html'
    form_class = InvoiceCreateForm
    extras_form_class = ExtrasUpdateForm
    success_url = reverse_lazy('invoice-list')
    success_message = _("Invoice was updated successfully")

    @method_decorator(login_required)
    @method_decorator(permission_required('invoice.change_invoice', login_url='/dashboard/'))
    def dispatch(self, request, *args, **kwargs):
        return super(InvoiceUpdate, self).dispatch(request)

    def get_context_data(self, **kwargs):
        context = super(InvoiceUpdate, self).get_context_data(**kwargs)
        context['company_account'] = self.request.user.company_account
        context['form'].fields['aircraft'].queryset = context['company_account'].aircrafts.all()
        context['form'].fields['pilot'].queryset = context['company_account'].company_pilots()
        if self.request.POST:
            context['extras_formset'] = inlineformset_factory(
                Invoice, Extras, form=self.extras_form_class, extra=0, can_delete=True)(self.request.POST,
                                                                                        instance=self.get_object())
        else:
            context['extras_formset'] = inlineformset_factory(
                Invoice, Extras, form=self.extras_form_class, extra=0, can_delete=True)(instance=self.get_object())
        return context

    def form_valid(self, form):
        context = self.get_context_data(form=form)
        formset = context['extras_formset']
        if formset.is_valid():
            self.object = form.save()
            formset.save()
            messages.success(self.request, self.success_message)
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form))


class InvoiceDelete(DeleteView):
    model = Invoice
    success_url = reverse_lazy('invoice-list')
    success_message = _("Invoice was deleted successfully")

    @method_decorator(login_required)
    @method_decorator(permission_required('invoice.delete_invoice', login_url='/dashboard/'))
    def dispatch(self, request, *args, **kwargs):
        return super(InvoiceDelete, self).dispatch(request)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.success_url)


# API Views

class AircraftDetailsAPI(APIView):
    def get(self, request, pk=None):
        try:
            aircraft = self.request.user.company_account.aircrafts.get(pk=pk)
            return Response(AircraftDetailsSerializer(aircraft).data, status=status.HTTP_200_OK)
        except Aircraft.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
