from django import forms
from django.utils.translation import ugettext as _
from django.core.exceptions import ValidationError

from datetimewidget.widgets import DateTimeWidget, DateWidget

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, HTML, Fieldset
from crispy_forms.bootstrap import Div, FormActions, InlineCheckboxes

from .models import Invoice, Extras


class ExtrasCreateForm(forms.ModelForm):
    class Meta:
        model = Extras
        fields = ('quantity', 'description', 'unit_price', )
        widgets = {
            'description': forms.Textarea(attrs={'rows': 1}),
        }

    def __init__(self, *args, **kwargs):
        super(ExtrasCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(Field('description'), css_class="col-md-6"),
                Div(Field('quantity'), css_class="col-md-3"),
                Div(Field('unit_price'), css_class="col-md-3"),
                css_class="row"
            ),
        )


class ExtrasUpdateForm(forms.ModelForm):
    class Meta:
        model = Extras
        fields = ('id', 'quantity', 'description', 'unit_price', )
        widgets = {
            'description': forms.Textarea(attrs={'rows': 1}),
        }

    def __init__(self, *args, **kwargs):
        super(ExtrasUpdateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(Field('description'), css_class="col-md-6"),
                Div(Field('quantity'), css_class="col-md-2"),
                Div(Field('unit_price'), css_class="col-md-2"),
                Div(Field('DELETE'), css_class="col-md-2 extras-delete-checkbox"),
                css_class="row"
            ),
        )

class InvoiceCreateForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ('id', 'company_account', 'customer_name', 'customer_address', 'customer_zip', 'customer_city',
                  'event_title', 'event_start', 'event_end', 'pilot_name', 'aircraft_mark', 'aircraft_type',
                  'passengers_number', 'aircraft_takeoff_cost', 'pilot', 'aircraft', 'status', 'date_created',
                  'due_date', 'tax', 'discount', 'comment')
        widgets = {
            'event_start': DateTimeWidget(bootstrap_version=3, options={
                'minuteStep': 15,
                'format': 'yyyy-mm-dd HH:ii',
                'pickerPosition': 'bottom-left',
            }),
            'event_end': DateTimeWidget(bootstrap_version=3, options={
                'minuteStep': 15,
                'format': 'yyyy-mm-dd HH:ii',
                'pickerPosition': 'bottom-left',
            }),
            'date_created': DateWidget(bootstrap_version=3, options={
                'format': 'yyyy-mm-dd',
            }),
            'due_date': DateWidget(bootstrap_version=3, options={
                'format': 'yyyy-mm-dd',
            }),
            'comment': forms.Textarea(attrs={'rows': 3, }),
        }

    def __init__(self, *args, **kwargs):
        super(InvoiceCreateForm, self).__init__(*args, **kwargs)
        self.fields['company_account'].widget = forms.HiddenInput()
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset(
                _('Customer'),
                Field('company_account'),
                Div(
                    Div(Field('customer_name'), css_class='col-md-6'),
                    css_class='row'
                ),
                Div(
                    Div(Field('customer_address'), css_class='col-md-4'),
                    Div(Field('customer_zip'), css_class='col-md-4'),
                    Div(Field('customer_city'), css_class='col-md-4'),
                    css_class='row'
                ),
            ),
            Fieldset(
                _('Event'),
                Div(
                    Div(Field('event_title'), css_class='col-md-4 disabled'),
                    Div(Field('event_start'), css_class='col-md-4'),
                    Div(Field('event_end'), css_class='col-md-4'),
                    css_class='row'
                ),
            ),
            Fieldset(
                _('Aircraft'),
                Div(
                    Div(Field('pilot_name'), css_class='col-md-4'),
                    Div(Field('aircraft_mark'), css_class='col-md-4'),
                    Div(Field('aircraft_type'), css_class='col-md-4'),
                    css_class='row'
                ),
                Div(
                    Div(Field('passengers_number'), css_class='col-md-4'),
                    Div(Field('aircraft_takeoff_cost'), css_class='col-md-4'),
                    css_class='row'
                ),
                Fieldset(
                    _('References'),
                    HTML("<p>Get pilot and aircraft data from selected</p>"),
                    Div(
                        Div(Field('pilot'), css_class='col-md-6'),
                        Div(Field('aircraft'), css_class='col-md-6'),
                        css_class='row'
                    ),
                    css_class='col-md-12'
                )
            ),
            Fieldset(
                _('Invoice information'),
                Div(
                    Div(Field('status'), css_class='col-md-4'),
                    Div(Field('date_created'), css_class='col-md-4'),
                    Div(Field('due_date'), css_class='col-md-4'),
                    css_class='row'
                ),
                Div(
                    Div(Field('tax'), css_class='col-md-3'),
                    Div(Field('discount'), css_class='col-md-3'),
                    Div(Field('comment'), css_class='col-md-6'),
                    css_class='row'
                ),
            ),
        )

    def clean(self):
        cleaned_data = super(InvoiceCreateForm, self).clean()

        if ('event_start' or 'event_end') not in cleaned_data:
            return

        if cleaned_data['event_start'] > cleaned_data['event_end']:
            raise ValidationError({'event_end': _('The end time must be later than the start time.')})
