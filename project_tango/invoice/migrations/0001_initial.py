# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import project_tango.invoice.models
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('aircraft', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Extras',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField(default=1)),
                ('description', models.TextField()),
                ('unit_price', models.DecimalField(max_digits=17, default=0.0, decimal_places=4)),
            ],
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('customer_name', models.CharField(max_length=64)),
                ('customer_address', models.CharField(max_length=64)),
                ('customer_zip', models.CharField(max_length=10, blank=True, default='')),
                ('customer_city', models.CharField(max_length=64)),
                ('event_title', models.CharField(max_length=255)),
                ('event_start', models.DateTimeField()),
                ('event_end', models.DateTimeField()),
                ('aircraft_mark', models.CharField(max_length=20)),
                ('aircraft_takeoff_cost', models.DecimalField(max_digits=17, decimal_places=4)),
                ('aircraft_type', models.CharField(max_length=255)),
                ('pilot_name', models.CharField(max_length=128, blank=True)),
                ('date_created', models.DateField(default=project_tango.invoice.models._date_created_today)),
                ('due_date', models.DateField(verbose_name='Payment due date', default=project_tango.invoice.models._date_due_week)),
                ('status', models.IntegerField(choices=[(0, 'Entered'), (1, 'Approved'), (2, 'Cancel'), (3, 'Paid')], default=0)),
                ('discount', models.DecimalField(max_digits=5, default=0.0, decimal_places=2)),
                ('passengers_number', models.PositiveIntegerField(default=0)),
                ('comment', models.TextField(null=True, blank=True)),
                ('aircraft', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, related_name='invoice_aircraft', blank=True, null=True, to='aircraft.Aircraft')),
                ('company_account', models.ForeignKey(to='core.CompanyAccount')),
                ('pilot', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, related_name='invoice_pilot', blank=True, null=True, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Tax',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('value', models.DecimalField(max_digits=5, default=10.0, decimal_places=2)),
                ('active', models.BooleanField(default=False)),
            ],
        ),
        migrations.AddField(
            model_name='invoice',
            name='tax',
            field=models.ForeignKey(to='invoice.Tax'),
        ),
        migrations.AddField(
            model_name='extras',
            name='invoice',
            field=models.ForeignKey(related_name='invoice_extras', to='invoice.Invoice'),
        ),
    ]
