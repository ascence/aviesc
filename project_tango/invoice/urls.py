from django.conf.urls import include, url

from .views import InvoiceList, InvoiceCreate, InvoiceUpdate, InvoiceDelete, AircraftDetailsAPI

urlpatterns = [
    url(r'^$', InvoiceList.as_view(), name="invoice-list"),
    url(r'^create/$', InvoiceCreate.as_view(), name="invoice-create"),
    url(r'^(?P<pk>[\w]+)/update/$', InvoiceUpdate.as_view(), name="invoice-update"),
    url(r'^(?P<pk>[\w]+)/delete/$', InvoiceDelete.as_view(), name="invoice-delete"),
    # api
    url(r'^api/aircraft/(?P<pk>[\w]+)/$', AircraftDetailsAPI.as_view(), name="api-aircraft-details"),
]
