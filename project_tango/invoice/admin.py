from django.contrib import admin

from .models import Invoice, Extras, Tax


class ExtrasInline(admin.TabularInline):
    model = Extras
    extra = 1


class InvoiceAdmin(admin.ModelAdmin):
    inlines = [
        ExtrasInline,
    ]


admin.site.register(Invoice, InvoiceAdmin)
admin.site.register([Tax, ])
