#!/bin/bash

NAME="aviesc_app"                                 # Name of the application
DJANGODIR=/home/ubuntu/aviesc                     # Django project directory
SOCKFILE=/home/ubuntu/aviesc/run/gunicorn.sock    # we will communicte using this unix socket
USER=ubuntu                                       # the user to run as
GROUP=ubuntu                                      # the group to run as
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=aviesc.production          # which settings file should Django use
DJANGO_WSGI_MODULE=aviesc.wsgi                    # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /home/ubuntu/.virtualenvs/aviesc/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --bind=0.0.0.0:8888 \
  --log-level=info \
  --log-file=-